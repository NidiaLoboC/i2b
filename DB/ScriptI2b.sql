PGDMP     '                
    x            I2b    12.2    12.2 K    �           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                      false            �           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                      false            �           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                      false            �           1262    17627    I2b    DATABASE     �   CREATE DATABASE "I2b" WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'Spanish_Spain.1252' LC_CTYPE = 'Spanish_Spain.1252';
    DROP DATABASE "I2b";
                postgres    false                        2615    2200    public    SCHEMA        CREATE SCHEMA public;
    DROP SCHEMA public;
                postgres    false            �           0    0    SCHEMA public    COMMENT     6   COMMENT ON SCHEMA public IS 'standard public schema';
                   postgres    false    3            �            1255    25629 T   fr_consulta_pelicula(character varying, integer, integer, integer, integer, integer)    FUNCTION     �  CREATE FUNCTION public.fr_consulta_pelicula(p_token character varying, p_id_usuario integer, p_id_pelicula integer, p_id_estado integer, p_id_actor integer, p_id_director integer) RETURNS TABLE(id_pelicula integer, nombre character varying, descripcion character varying, num_peliculas integer, numero_disponibles integer, prestadas integer, disponibilidad boolean)
    LANGUAGE plpgsql
    AS $$
	DECLARE
	
BEGIN

	--IF EXISTS(SELECT ST.ID_TOKEN FROM SOL_TOKEN ST WHERE ST.TOKEN=P_TOKEN AND ST.ID_USUARIO=P_ID_USUARIO AND ST.VIGENTE=TRUE) THEN
		
		RETURN QUERY SELECT DISTINCT SP.ID_PELICULA, SP.NOMBRE,SP.DESCRIPCION, SP.NUM_PELICULAS, COALESCE(SP.NUM_DISPONIBLES,0) AS NUMERO_DISPONIBLES,
		COALESCE(SP.NUM_PRESTADAS,0) AS PRESTADAS,
						CASE WHEN SP.NUM_DISPONIBLES<=SP.NUM_PELICULAS AND SP.NUM_DISPONIBLES>0 THEN TRUE
						ELSE FALSE
						END AS DISPONIBILIDAD
						FROM SOL_PELICULAS SP LEFT JOIN SOL_RELA_PELIS_ACTOR SRPA ON SP.ID_PELICULA=SRPA.ID_PELICULA AND SRPA.VIGENTE=TRUE
						LEFT JOIN MA_ACTORES MA ON SRPA.ID_ACTOR=MA.ID_ACTOR
						LEFT JOIN MA_DIRECTORES MD ON MD.ID_DIRECTOR=SP.ID_DIRECTOR
						LEFT JOIN MA_ESTADOS ME ON ME.ID_ESTADO=SP.ID_ESTADO
						WHERE SP.VIGENTE=TRUE AND 
						(P_ID_PELICULA =0 OR SP.ID_PELICULA=P_ID_PELICULA)AND 
						(P_ID_ESTADO =0 OR ME.ID_ESTADO=P_ID_ESTADO)AND 
						(P_ID_DIRECTOR =0 OR MD.ID_DIRECTOR=P_ID_DIRECTOR)AND 
						(P_ID_ACTOR =0 OR SRPA.ID_ACTOR=P_ID_ACTOR);   -- OPEN A CURSOR
			  	
	--END IF;
	
	END;
$$;
 �   DROP FUNCTION public.fr_consulta_pelicula(p_token character varying, p_id_usuario integer, p_id_pelicula integer, p_id_estado integer, p_id_actor integer, p_id_director integer);
       public          postgres    false    3            �            1255    25653 r   fr_consultar_ordenes(character varying, integer, character varying, integer, character varying, character varying)    FUNCTION       CREATE FUNCTION public.fr_consultar_ordenes(p_token character varying, p_id_usuario integer, p_id_orden character varying, p_id_estado integer, p_fecha_inicio character varying, p_fecha_fin character varying) RETURNS TABLE(id_orden integer, orden character varying, pelicula character varying, usuario character varying, estado character varying)
    LANGUAGE plpgsql
    AS $$
	DECLARE
	
BEGIN

	IF EXISTS(SELECT ST.ID_TOKEN FROM SOL_TOKEN ST WHERE ST.TOKEN=P_TOKEN AND ST.ID_USUARIO=P_ID_USUARIO AND ST.VIGENTE=TRUE) THEN
		
		RETURN QUERY SELECT SO.ID_ORDEN,SO.CODIGO_ORDEN AS ORDEN,SP.NOMBRE AS PELICULA,SU.NOMBRE AS USUARIO,ME.DESCRIPCION AS ESTADO
						FROM SOL_ORNEDES AS SO
						LEFT JOIN SOL_PELICULAS SP ON SP.ID_PELICULA=SO.ID_PELICULA
						LEFT JOIN MA_ESTADOS ME ON ME.ID_ESTADO=SO.ID_ESTADO
						LEFT JOIN SOL_USUARIO SU ON SU.ID_USUARIO=SO.ID_USUARIO
						WHERE SO.VIGENTE=TRUE AND 
						(P_ID_USUARIO =0 OR SU.ID_USUARIO=P_ID_USUARIO) AND
						(P_ID_ORDEN IS NULL OR SO.CODIGO_ORDEN=P_ID_ORDEN) AND
						(P_FECHA_INICIO IS NULL OR SO.FEC_CREA >= TO_TIMESTAMP(P_FECHA_INICIO||'00:0', 'DD-MM-YYYY SS:MS') OR SO.FEC_CREA <=  TO_TIMESTAMP(P_FECHA_FIN||'00:0', 'DD-MM-YYYY SS:MS') ) AND
						(P_ID_ESTADO =0 OR SO.ID_ESTADO=P_ID_ESTADO);   -- OPEN A CURSOR
			  	
	END IF;
	
	END;
$$;
 �   DROP FUNCTION public.fr_consultar_ordenes(p_token character varying, p_id_usuario integer, p_id_orden character varying, p_id_estado integer, p_fecha_inicio character varying, p_fecha_fin character varying);
       public          postgres    false    3            �            1255    17848 5   fr_detalle_orden(character varying, integer, integer)    FUNCTION       CREATE FUNCTION public.fr_detalle_orden(p_token character varying, p_id_usuario integer, p_id_orden integer) RETURNS TABLE(codigo_orden character varying, estado_anterior character varying, estado_actual character varying, fecha_modificacion timestamp with time zone, num_prestadas integer)
    LANGUAGE plpgsql
    AS $$
	DECLARE
	
BEGIN

	IF EXISTS(SELECT ST.ID_TOKEN FROM SOL_TOKEN ST WHERE ST.TOKEN=P_TOKEN AND ST.ID_USUARIO=P_ID_USUARIO AND ST.VIGENTE=TRUE) THEN
		
		RETURN QUERY SELECT SO.CODIGO_ORDEN, COALESCE(MSA.DESCRIPCION,'') AS ESTADO_ANTERIOR, MS.DESCRIPCION AS ESTADO_ACTUAL,
						SH.FEC_CREA AS FECHA_MODIFICACION, SO.NUM_PRESTADAS
						FROM SOL_HISTORICO_PELIS_ORDEN SH INNER JOIN SOL_ORNEDES SO ON SO.ID_ORDEN=SH.ID_ORDEN
						INNER JOIN MA_ESTADOS MS ON SH.ID_ESTADO_ACTUAL=MS.ID_ESTADO AND MS.VIGENTE =TRUE
						LEFT JOIN MA_ESTADOS MSA ON SH.ID_ESTADO_ANTERIOR=MSA.ID_ESTADO AND MSA.VIGENTE=TRUE
						WHERE SH.VIGENTE=TRUE AND 
						(P_ID_ORDEN =0 OR SO.ID_ORDEN=P_ID_ORDEN);   -- OPEN A CURSOR
			  	
	END IF;
	
	END;
$$;
 l   DROP FUNCTION public.fr_detalle_orden(p_token character varying, p_id_usuario integer, p_id_orden integer);
       public          postgres    false    3            �            1255    17811 /   fr_historico_ordenes(integer, integer, integer)    FUNCTION       CREATE FUNCTION public.fr_historico_ordenes(p_id_orden integer, p_id_estado_actual integer, p_id_estado_anterior integer, OUT p_id_historico integer) RETURNS integer
    LANGUAGE plpgsql
    AS $$DECLARE
	
BEGIN

	WITH UPD AS (INSERT INTO PUBLIC.SOL_HISTORICO_PELIS_ORDEN(
	ID_ORDEN, ID_ESTADO_ACTUAL, ID_ESTADO_ANTERIOR, VIGENTE, USU_CREA, FEC_CREA)
	VALUES ( P_ID_ORDEN, P_ID_ESTADO_ACTUAL, P_ID_ESTADO_ANTERIOR, TRUE, USER, NOW()) RETURNING ID_HISTORICO )
	SELECT INTO P_ID_HISTORICO ID_HISTORICO FROM UPD;
	
END$$;
 �   DROP FUNCTION public.fr_historico_ordenes(p_id_orden integer, p_id_estado_actual integer, p_id_estado_anterior integer, OUT p_id_historico integer);
       public          postgres    false    3            �            1255    17770 =   fr_listar_actor(character varying, integer, integer, integer)    FUNCTION     �  CREATE FUNCTION public.fr_listar_actor(p_token character varying, p_id_usuario integer, p_id_pelicula integer, p_id_actores integer) RETURNS TABLE(id_actor integer, nombre character varying)
    LANGUAGE plpgsql
    AS $$
	DECLARE
	
BEGIN

	--IF EXISTS(SELECT ST.ID_TOKEN FROM SOL_TOKEN ST WHERE ST.TOKEN=P_TOKEN AND ST.ID_USUARIO=P_ID_USUARIO AND ST.VIGENTE=TRUE) THEN
		
		RETURN QUERY SELECT MA.ID_ACTOR,MA.NOMBRE
					FROM MA_ACTORES MA INNER JOIN SOL_RELA_PELIS_ACTOR SRPA ON SRPA.ID_ACTOR=MA.ID_ACTOR
					INNER JOIN SOL_PELICULAS SP ON SP.ID_PELICULA=SRPA.ID_PELICULA AND SRPA.VIGENTE=TRUE
					WHERE (P_ID_PELICULA =0 OR SRPA.ID_PELICULA=P_ID_PELICULA) AND
					(P_ID_ACTORES =0 OR MA.ID_ACTOR=P_ID_ACTORES) ;   -- OPEN A CURSOR
			  	
--	END IF;
	
	END;
$$;
 �   DROP FUNCTION public.fr_listar_actor(p_token character varying, p_id_usuario integer, p_id_pelicula integer, p_id_actores integer);
       public          postgres    false    3            �            1255    17771 7   fr_listar_director(character varying, integer, integer)    FUNCTION     :  CREATE FUNCTION public.fr_listar_director(p_token character varying, p_id_usuario integer, p_id_director integer) RETURNS TABLE(id_director integer, nombre character varying)
    LANGUAGE plpgsql
    AS $$
	DECLARE
	
BEGIN

--	IF EXISTS(SELECT ST.ID_TOKEN FROM SOL_TOKEN ST WHERE ST.TOKEN=P_TOKEN AND ST.ID_USUARIO=P_ID_USUARIO AND ST.VIGENTE=TRUE) THEN
		
		RETURN QUERY SELECT MD.ID_DIRECTOR,MD.NOMBRE
					FROM MA_DIRECTORES MD
					WHERE MD.VIGENTE=TRUE AND
					(P_ID_DIRECTOR =0 OR MD.ID_DIRECTOR= P_ID_DIRECTOR) ;   -- OPEN A CURSOR
			  	
--	END IF;
	
	END;
$$;
 q   DROP FUNCTION public.fr_listar_director(p_token character varying, p_id_usuario integer, p_id_director integer);
       public          postgres    false    3            �            1255    17772 5   fr_listar_estado(character varying, integer, integer)    FUNCTION     a  CREATE FUNCTION public.fr_listar_estado(p_token character varying, p_id_usuario integer, p_id_estado integer) RETURNS TABLE(id_estado integer, descripcion character varying)
    LANGUAGE plpgsql
    AS $$
	DECLARE
	
BEGIN

	IF EXISTS(SELECT ST.ID_TOKEN FROM SOL_TOKEN ST WHERE ST.TOKEN=P_TOKEN AND ST.ID_USUARIO=P_ID_USUARIO AND ST.VIGENTE=TRUE) THEN
		
		RETURN QUERY SELECT MA.ID_ESTADO,MA.DESCRIPCION
					FROM MA_ESTADOS MA
					WHERE  MA.VIGENTE=TRUE AND MA.TIPO=2 AND 
					(P_ID_ESTADO =0 OR MA.ID_ESTADO= P_ID_ESTADO) ;   -- OPEN A CURSOR --TIPO 2 ES ORDEN TIPO 1 PELICULA
			  	
	END IF;
	
	END;
$$;
 m   DROP FUNCTION public.fr_listar_estado(p_token character varying, p_id_usuario integer, p_id_estado integer);
       public          postgres    false    3            �            1255    17845 7   fr_listar_pelicula(character varying, integer, integer)    FUNCTION     <  CREATE FUNCTION public.fr_listar_pelicula(p_token character varying, p_id_usuario integer, p_id_pelicula integer) RETURNS TABLE(id_pelicula integer, nombre character varying)
    LANGUAGE plpgsql
    AS $$
	DECLARE
	
BEGIN

	--IF EXISTS(SELECT ST.ID_TOKEN FROM SOL_TOKEN ST WHERE ST.TOKEN=P_TOKEN AND ST.ID_USUARIO=P_ID_USUARIO AND ST.VIGENTE=TRUE) THEN
		
		RETURN QUERY SELECT SP.ID_PELICULA, SP.NOMBRE
						FROM SOL_PELICULAS SP
						WHERE SP.VIGENTE=TRUE AND 
						(P_ID_PELICULA=0 OR SP.ID_PELICULA=P_ID_PELICULA);   -- OPEN A CURSOR
			  	
--	END IF;
	
	END;
$$;
 q   DROP FUNCTION public.fr_listar_pelicula(p_token character varying, p_id_usuario integer, p_id_pelicula integer);
       public          postgres    false    3            �            1255    17840 j   pr_auditoria(integer, character varying, character varying, character varying, character varying, boolean) 	   PROCEDURE     z  CREATE PROCEDURE public.pr_auditoria(p_errorcode integer, p_msjerror character varying, p_methoderror character varying, p_classerror character varying, INOUT p_message_out character varying, INOUT p_eserror_out boolean)
    LANGUAGE plpgsql
    AS $$DECLARE
  --V_CURSOR refcursor;                                
  -- Declare a cursor variable  
BEGIN
	
	P_ESERROR_OUT := false;
	P_MESSAGE_OUT := 'Registro guardado con exitoso.';
	
	INSERT INTO PUBLIC.TA_AUDITORIA(
	 CODIGOERROR, MSJERROR, METODO,CLASSERROR, VIGENTE, USU_CREA, FEC_CREA)
	VALUES (P_ERRORCODE, P_MSJERROR, P_METHODERROR, P_CLASSERROR, TRUE, USER, NOW());
	
END$$;
 �   DROP PROCEDURE public.pr_auditoria(p_errorcode integer, p_msjerror character varying, p_methoderror character varying, p_classerror character varying, INOUT p_message_out character varying, INOUT p_eserror_out boolean);
       public          postgres    false    3            �            1255    17814 o   pr_creacion_reserva(character varying, integer, integer, integer, integer, integer, character varying, boolean) 	   PROCEDURE     �
  CREATE PROCEDURE public.pr_creacion_reserva(p_token character varying, p_id_usuario integer, p_id_pelicula integer, p_numero_reserva integer, p_id_estado integer, INOUT p_id_orden integer, INOUT p_message_out character varying, INOUT p_eserror_out boolean)
    LANGUAGE plpgsql
    AS $$DECLARE
  --V_CURSOR refcursor;                                
  -- Declare a cursor variable  
  RESULTADO INTEGER:= 0;
BEGIN
	
	P_ESERROR_OUT := false;
	P_MESSAGE_OUT := 'Creación de reserva con exito.';
	
	
	IF EXISTS(SELECT ST.ID_TOKEN FROM SOL_TOKEN ST WHERE ST.TOKEN=P_TOKEN AND ST.ID_USUARIO=P_ID_USUARIO AND ST.VIGENTE=TRUE) THEN
	
		IF EXISTS(SELECT NUM_DISPONIBLES FROM SOL_PELICULAS WHERE VIGENTE=TRUE AND NUM_DISPONIBLES<=NUM_PELICULAS AND 
				  NUM_DISPONIBLES>0 AND ID_PELICULA=P_ID_PELICULA) THEN
	
			IF EXISTS(SELECT NUM_DISPONIBLES FROM SOL_PELICULAS WHERE VIGENTE=TRUE AND NUM_DISPONIBLES<=NUM_PELICULAS AND 
					  NUM_DISPONIBLES>0 AND NUM_DISPONIBLES>=P_NUMERO_RESERVA AND ID_PELICULA=P_ID_PELICULA) THEN
					  
					  
			UPDATE PUBLIC.SOL_PELICULAS SP
				SET NUM_DISPONIBLES=(SELECT (SPU.NUM_DISPONIBLES-P_NUMERO_RESERVA) FROM SOL_PELICULAS SPU WHERE SPU.ID_PELICULA=P_ID_PELICULA), 
				NUM_PRESTADAS=(SELECT (SPU.NUM_PRESTADAS+P_NUMERO_RESERVA) FROM SOL_PELICULAS SPU WHERE SPU.ID_PELICULA=P_ID_PELICULA),
				ID_ESTADO=(SELECT CASE WHEN (SPU.NUM_DISPONIBLES-P_NUMERO_RESERVA)=0 THEN 2 ELSE 1 END FROM SOL_PELICULAS SPU WHERE SPU.ID_PELICULA=P_ID_PELICULA)
				WHERE SP.ID_PELICULA=P_ID_PELICULA;
		
			WITH UPD AS (INSERT INTO PUBLIC.SOL_ORNEDES(
				CODIGO_ORDEN, ID_PELICULA, ID_USUARIO, NUM_PRESTADAS, ID_ESTADO, VIGENTE,USU_CREA, FEC_CREA)
				VALUES ((SELECT 'O'||FLOOR(RANDOM()* (11) + 5)||''|| TO_CHAR(NOW() , 'YYYYMMDDHH24MISSMS')  ||''||FLOOR(RANDOM()* (11) + 5)), 
				P_ID_PELICULA, 
				P_ID_USUARIO,
				P_NUMERO_RESERVA,
				P_ID_ESTADO, 
				TRUE, 
				USER,
				NOW()) RETURNING ID_ORDEN )
				SELECT INTO P_ID_ORDEN ID_ORDEN FROM UPD;
				
				IF P_id_orden IS NULL THEN	
				P_ESERROR_OUT := TRUE;
				P_MESSAGE_OUT := 'No fue posible crear la orden.';	
				
				else
				
				RESULTADO:= (SELECT FR_HISTORICO_ORDENES(P_ID_ORDEN,P_ID_ESTADO,
				(SELECT COALESCE(ID_ESTADO_ANTERIOR,0) FROM SOL_HISTORICO_PELIS_ORDEN WHERE ID_ORDEN=P_ID_ORDEN AND VIGENTE=TRUE)));
				
				END IF;
					
			ELSE
			
				P_ESERROR_OUT := TRUE;
				P_MESSAGE_OUT := 'Cantidades de unidiades a prestar supera el número disponible.'; 
				
			END IF;
				
		ELSE
		
			P_ESERROR_OUT := TRUE;
			P_MESSAGE_OUT := 'La pelicula que intenta reservar no cuenta con unidades disponibles, intente nuevamente en unos minutos.'; 
			
		END IF;
		
	ELSE
	
		P_ESERROR_OUT := TRUE;
		P_MESSAGE_OUT := 'Este usuario ya está creado.'; 
		
	END IF;
		
	--RETURN 0;
	
END$$;
    DROP PROCEDURE public.pr_creacion_reserva(p_token character varying, p_id_usuario integer, p_id_pelicula integer, p_numero_reserva integer, p_id_estado integer, INOUT p_id_orden integer, INOUT p_message_out character varying, INOUT p_eserror_out boolean);
       public          postgres    false    3            �            1255    25640 �   pr_creacion_reserva(character varying, integer, integer, integer, integer, integer, character varying, character varying, boolean) 	   PROCEDURE       CREATE PROCEDURE public.pr_creacion_reserva(p_token character varying, p_id_usuario integer, p_id_pelicula integer, p_numero_reserva integer, p_id_estado integer, INOUT p_id_orden integer, INOUT codorden character varying, INOUT p_message_out character varying, INOUT p_eserror_out boolean)
    LANGUAGE plpgsql
    AS $$DECLARE
  --V_CURSOR refcursor;                                
  -- Declare a cursor variable  
  RESULTADO INTEGER:= 0;
BEGIN
	
	P_ESERROR_OUT := false;
	P_MESSAGE_OUT := 'Creación de reserva con exito.';
	
	
	IF EXISTS(SELECT ST.ID_TOKEN FROM SOL_TOKEN ST WHERE ST.TOKEN=P_TOKEN AND ST.ID_USUARIO=P_ID_USUARIO AND ST.VIGENTE=TRUE) THEN
	
		IF EXISTS(SELECT NUM_DISPONIBLES FROM SOL_PELICULAS WHERE VIGENTE=TRUE AND NUM_DISPONIBLES<=NUM_PELICULAS AND 
				  NUM_DISPONIBLES>0 AND ID_PELICULA=P_ID_PELICULA) THEN
	
			IF EXISTS(SELECT NUM_DISPONIBLES FROM SOL_PELICULAS WHERE VIGENTE=TRUE AND NUM_DISPONIBLES<=NUM_PELICULAS AND 
					  NUM_DISPONIBLES>0 AND NUM_DISPONIBLES>=P_NUMERO_RESERVA AND ID_PELICULA=P_ID_PELICULA) THEN
					  
					  
			UPDATE PUBLIC.SOL_PELICULAS SP
				SET NUM_DISPONIBLES=(SELECT (SPU.NUM_DISPONIBLES-P_NUMERO_RESERVA) FROM SOL_PELICULAS SPU WHERE SPU.ID_PELICULA=P_ID_PELICULA), 
				NUM_PRESTADAS=(SELECT (SPU.NUM_PRESTADAS+P_NUMERO_RESERVA) FROM SOL_PELICULAS SPU WHERE SPU.ID_PELICULA=P_ID_PELICULA),
				ID_ESTADO=(SELECT CASE WHEN (SPU.NUM_DISPONIBLES-P_NUMERO_RESERVA)=0 THEN 2 ELSE 1 END FROM SOL_PELICULAS SPU WHERE SPU.ID_PELICULA=P_ID_PELICULA)
				WHERE SP.ID_PELICULA=P_ID_PELICULA;
				
				CODORDEN:=(SELECT 'O'||FLOOR(RANDOM()* (11) + 5)||''|| TO_CHAR(NOW() , 'YYYYMMDDHH24MISSMS')  ||''||FLOOR(RANDOM()* (11) + 5));
		
			WITH UPD AS (INSERT INTO PUBLIC.SOL_ORNEDES(
				CODIGO_ORDEN, ID_PELICULA, ID_USUARIO, NUM_PRESTADAS, ID_ESTADO, VIGENTE,USU_CREA, FEC_CREA)
				VALUES (CODORDEN, 
				P_ID_PELICULA, 
				P_ID_USUARIO,
				P_NUMERO_RESERVA,
				P_ID_ESTADO, 
				TRUE, 
				USER,
				NOW()) RETURNING ID_ORDEN )
				SELECT INTO P_ID_ORDEN ID_ORDEN FROM UPD;
				
				IF P_id_orden IS NULL THEN	
				P_ESERROR_OUT := TRUE;
				P_MESSAGE_OUT := 'No fue posible crear la orden.';	
				
				else
				
				RESULTADO:= (SELECT FR_HISTORICO_ORDENES(P_ID_ORDEN,P_ID_ESTADO,
				(SELECT COALESCE(ID_ESTADO_ANTERIOR,0) FROM SOL_HISTORICO_PELIS_ORDEN WHERE ID_ORDEN=P_ID_ORDEN AND VIGENTE=TRUE)));
				
				END IF;
					
			ELSE
			
				P_ESERROR_OUT := TRUE;
				P_MESSAGE_OUT := 'Cantidades de unidiades a prestar supera el número disponible.'; 
				
			END IF;
				
		ELSE
		
			P_ESERROR_OUT := TRUE;
			P_MESSAGE_OUT := 'La pelicula que intenta reservar no cuenta con unidades disponibles, intente nuevamente en unos minutos.'; 
			
		END IF;
		
	ELSE
	
		P_ESERROR_OUT := TRUE;
		P_MESSAGE_OUT := 'Este usuario ya está creado.'; 
		
	END IF;
		
	--RETURN 0;
	
END$$;
 "  DROP PROCEDURE public.pr_creacion_reserva(p_token character varying, p_id_usuario integer, p_id_pelicula integer, p_numero_reserva integer, p_id_estado integer, INOUT p_id_orden integer, INOUT codorden character varying, INOUT p_message_out character varying, INOUT p_eserror_out boolean);
       public          postgres    false    3            �            1255    17799 �   pr_creacion_usuario(character varying, character varying, character varying, character varying, integer, character varying, character varying, integer, character varying, boolean) 	   PROCEDURE     �  CREATE PROCEDURE public.pr_creacion_usuario(p_rut character varying, p_nombre character varying, p_direccion character varying, p_telefono character varying, p_id_tipo_usuario integer, p_user character varying, p_pass character varying, INOUT p_id_usuario integer, INOUT p_message_out character varying, INOUT p_eserror_out boolean)
    LANGUAGE plpgsql
    AS $$DECLARE
  --V_CURSOR refcursor;                                
  -- Declare a cursor variable  
BEGIN
	
	P_ESERROR_OUT := false;
	P_MESSAGE_OUT := 'Creación de usuario exitoso.';
	
	IF EXISTS(SELECT ID_USUARIO FROM SOL_USUARIO WHERE RUT=P_RUT AND VIGENTE=TRUE) THEN
  	
		P_ESERROR_OUT := TRUE;
		P_MESSAGE_OUT := 'Este usuario ya está creado.'; 
				
	ELSE
	
		WITH UPD AS (INSERT INTO PUBLIC.SOL_USUARIO(NOMBRE, DIRECCION, TELEFONO, TIPO_USUARIO, RUT, USUARIO,
			CONTRASENA, VIGENTE, USU_CREA, FEC_CREA)
			VALUES ( P_NOMBRE, 
			P_DIRECCION, 
			P_TELEFONO,
			P_ID_TIPO_USUARIO, 
			P_RUT, 
			P_USER, 
			P_PASS,
			TRUE, 
			USER,
			NOW()) RETURNING ID_USUARIO)
			SELECT INTO P_ID_USUARIO ID_USUARIO FROM UPD;
			
			IF P_ID_USUARIO IS NULL THEN	
			P_ESERROR_OUT := TRUE;
			P_MESSAGE_OUT := 'No fue posible crear el usuario.';	
	 		
			END IF;
		
	END IF;
		
	--RETURN 0;
	
END$$;
 L  DROP PROCEDURE public.pr_creacion_usuario(p_rut character varying, p_nombre character varying, p_direccion character varying, p_telefono character varying, p_id_tipo_usuario integer, p_user character varying, p_pass character varying, INOUT p_id_usuario integer, INOUT p_message_out character varying, INOUT p_eserror_out boolean);
       public          postgres    false    3            �            1255    25642 y   pr_login(character varying, character varying, integer, character varying, character varying, character varying, boolean) 	   PROCEDURE       CREATE PROCEDURE public.pr_login(p_user character varying, p_pass character varying, INOUT p_id_usuario integer, INOUT p_name_usuario character varying, INOUT p_token character varying, INOUT p_message_out character varying, INOUT p_eserror_out boolean)
    LANGUAGE plpgsql
    AS $$DECLARE
  --V_CURSOR refcursor;                                
  -- Declare a cursor variable  
BEGIN
	
	P_ESERROR_OUT := false;
	P_MESSAGE_OUT := 'Inicio de sesion exitoso.';
	P_ID_USUARIO:=(SELECT ID_USUARIO FROM SOL_USUARIO WHERE USUARIO = P_USER AND CONTRASENA=P_PASS AND VIGENTE=TRUE);
	P_NAME_USUARIO:=(SELECT NOMBRE FROM SOL_USUARIO WHERE USUARIO = P_USER AND CONTRASENA=P_PASS AND VIGENTE=TRUE);

	
  IF (P_ID_USUARIO>0) THEN
  
	
		P_TOKEN:=(SELECT FLOOR(RANDOM()* (11) + 5)||''|| TO_CHAR(NOW() , 'YYYYMMDDHH24MISSMS')  ||''||FLOOR(RANDOM()* (11) + 5));
			 
		IF (P_TOKEN IS NOT NULL)THEN
		
			INSERT INTO PUBLIC.SOL_TOKEN(TOKEN, ID_USUARIO, VIGENTE, USU_CREA, FEC_CREA)
			VALUES (P_TOKEN, P_ID_USUARIO, TRUE, USER, NOW());
					
			
		ELSE
		
			P_ESERROR_OUT := TRUE;
			P_MESSAGE_OUT := 'Error al generar token.';
		
		END IF; 
				
	ELSE
	
		P_ESERROR_OUT := TRUE;
		P_MESSAGE_OUT := 'Usuario o contraseña incorrectos, por favor revisar'; 
		P_TOKEN:=NULL;
		
	END IF;
		
	--RETURN 0;
	
END$$;
 �   DROP PROCEDURE public.pr_login(p_user character varying, p_pass character varying, INOUT p_id_usuario integer, INOUT p_name_usuario character varying, INOUT p_token character varying, INOUT p_message_out character varying, INOUT p_eserror_out boolean);
       public          postgres    false    3            �            1255    17774 A   pr_logout(integer, character varying, character varying, boolean) 	   PROCEDURE     �  CREATE PROCEDURE public.pr_logout(p_id_usuario integer, p_token character varying, INOUT p_message_out character varying, INOUT p_eserror_out boolean)
    LANGUAGE plpgsql
    AS $$DECLARE
  --V_CURSOR refcursor;                                
  -- Declare a cursor variable  
BEGIN
	
	P_ESERROR_OUT := false;
	P_MESSAGE_OUT := 'Cierre de sesion exitoso.';
	
	UPDATE PUBLIC.SOL_TOKEN
	SET VIGENTE=FALSE
	WHERE ID_TOKEN=P_TOKEN AND ID_USUARIO=P_ID_USUARIO;
	
END$$;
 �   DROP PROCEDURE public.pr_logout(p_id_usuario integer, p_token character varying, INOUT p_message_out character varying, INOUT p_eserror_out boolean);
       public          postgres    false    3            �            1259    17640    ma_actores_id_actor_seq    SEQUENCE     �   CREATE SEQUENCE public.ma_actores_id_actor_seq
    START WITH 7
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 2147483647
    CACHE 1;
 .   DROP SEQUENCE public.ma_actores_id_actor_seq;
       public          postgres    false    3            �            1259    17710 
   ma_actores    TABLE     F  CREATE TABLE public.ma_actores (
    id_actor integer DEFAULT nextval('public.ma_actores_id_actor_seq'::regclass) NOT NULL,
    nombre character varying(20) NOT NULL,
    id_pelicula integer NOT NULL,
    vigente boolean NOT NULL,
    usu_crea character varying(50) NOT NULL,
    fec_crea timestamp with time zone NOT NULL
);
    DROP TABLE public.ma_actores;
       public         heap    postgres    false    208    3            �            1259    17638    ma_directores_id_director_seq    SEQUENCE     �   CREATE SEQUENCE public.ma_directores_id_director_seq
    START WITH 7
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 2147483647
    CACHE 1;
 4   DROP SEQUENCE public.ma_directores_id_director_seq;
       public          postgres    false    3            �            1259    17704    ma_directores    TABLE     0  CREATE TABLE public.ma_directores (
    id_director integer DEFAULT nextval('public.ma_directores_id_director_seq'::regclass) NOT NULL,
    nombre character varying(20) NOT NULL,
    vigente boolean NOT NULL,
    usu_crea character varying(50) NOT NULL,
    fec_crea timestamp with time zone NOT NULL
);
 !   DROP TABLE public.ma_directores;
       public         heap    postgres    false    207    3            �            1259    17636    ma_estados_id_estado_seq    SEQUENCE     �   CREATE SEQUENCE public.ma_estados_id_estado_seq
    START WITH 7
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 2147483647
    CACHE 1;
 /   DROP SEQUENCE public.ma_estados_id_estado_seq;
       public          postgres    false    3            �            1259    25644 
   ma_estados    TABLE     G  CREATE TABLE public.ma_estados (
    id_estado integer DEFAULT nextval('public.ma_estados_id_estado_seq'::regclass) NOT NULL,
    descripcion character varying(500) NOT NULL,
    tipo integer NOT NULL,
    vigente boolean NOT NULL,
    usu_crea character varying(50) NOT NULL,
    fec_crea timestamp with time zone NOT NULL
);
    DROP TABLE public.ma_estados;
       public         heap    postgres    false    206    3            �            1259    17787    ma_tipo_usuario_id_tipo_seq    SEQUENCE     �   CREATE SEQUENCE public.ma_tipo_usuario_id_tipo_seq
    START WITH 7
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 2147483647
    CACHE 1;
 2   DROP SEQUENCE public.ma_tipo_usuario_id_tipo_seq;
       public          postgres    false    3            �            1259    17789    ma_tipo_usuario    TABLE     1  CREATE TABLE public.ma_tipo_usuario (
    id_tipo integer DEFAULT nextval('public.ma_tipo_usuario_id_tipo_seq'::regclass) NOT NULL,
    descripcion character varying(20) NOT NULL,
    vigente boolean NOT NULL,
    usu_crea character varying(50) NOT NULL,
    fec_crea timestamp with time zone NOT NULL
);
 #   DROP TABLE public.ma_tipo_usuario;
       public         heap    postgres    false    215    3            �            1259    17634 *   sol_historico_pelis_orden_id_historico_seq    SEQUENCE     �   CREATE SEQUENCE public.sol_historico_pelis_orden_id_historico_seq
    START WITH 7
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 2147483647
    CACHE 1;
 A   DROP SEQUENCE public.sol_historico_pelis_orden_id_historico_seq;
       public          postgres    false    3            �            1259    17855    sol_historico_pelis_orden    TABLE     �  CREATE TABLE public.sol_historico_pelis_orden (
    id_historico integer DEFAULT nextval('public.sol_historico_pelis_orden_id_historico_seq'::regclass) NOT NULL,
    id_orden integer NOT NULL,
    id_estado_actual integer NOT NULL,
    id_estado_anterior integer,
    vigente boolean NOT NULL,
    usu_crea character varying(50) NOT NULL,
    fec_crea timestamp with time zone NOT NULL
);
 -   DROP TABLE public.sol_historico_pelis_orden;
       public         heap    postgres    false    205    3            �            1259    17632    sol_ornedes_id_orden_seq    SEQUENCE     �   CREATE SEQUENCE public.sol_ornedes_id_orden_seq
    START WITH 7
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 2147483647
    CACHE 1;
 /   DROP SEQUENCE public.sol_ornedes_id_orden_seq;
       public          postgres    false    3            �            1259    17801    sol_ornedes    TABLE     �  CREATE TABLE public.sol_ornedes (
    id_orden integer DEFAULT nextval('public.sol_ornedes_id_orden_seq'::regclass) NOT NULL,
    codigo_orden character varying(30) NOT NULL,
    id_pelicula integer NOT NULL,
    id_usuario integer NOT NULL,
    num_prestadas integer NOT NULL,
    id_estado integer NOT NULL,
    vigente boolean NOT NULL,
    usu_crea character varying(50) NOT NULL,
    fec_crea timestamp with time zone NOT NULL
);
    DROP TABLE public.sol_ornedes;
       public         heap    postgres    false    204    3            �            1259    17630    sol_peliculas_id_pelicula_seq    SEQUENCE     �   CREATE SEQUENCE public.sol_peliculas_id_pelicula_seq
    START WITH 7
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 2147483647
    CACHE 1;
 4   DROP SEQUENCE public.sol_peliculas_id_pelicula_seq;
       public          postgres    false    3            �            1259    25630    sol_peliculas    TABLE        CREATE TABLE public.sol_peliculas (
    id_pelicula integer DEFAULT nextval('public.sol_peliculas_id_pelicula_seq'::regclass) NOT NULL,
    nombre character varying(20) NOT NULL,
    descripcion character varying(1000) NOT NULL,
    num_peliculas integer NOT NULL,
    num_disponibles integer,
    num_prestadas integer,
    id_director integer NOT NULL,
    id_estado integer NOT NULL,
    vigente boolean NOT NULL,
    usu_crea character varying(50) NOT NULL,
    fec_crea timestamp with time zone NOT NULL
);
 !   DROP TABLE public.sol_peliculas;
       public         heap    postgres    false    203    3            �            1259    17628 (   sol_relacion_pelis_actor_id_relacion_seq    SEQUENCE     �   CREATE SEQUENCE public.sol_relacion_pelis_actor_id_relacion_seq
    START WITH 7
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 2147483647
    CACHE 1;
 ?   DROP SEQUENCE public.sol_relacion_pelis_actor_id_relacion_seq;
       public          postgres    false    3            �            1259    17754    sol_rela_pelis_actor    TABLE     X  CREATE TABLE public.sol_rela_pelis_actor (
    id_relacion integer DEFAULT nextval('public.sol_relacion_pelis_actor_id_relacion_seq'::regclass) NOT NULL,
    id_pelicula integer NOT NULL,
    id_actor integer NOT NULL,
    vigente boolean NOT NULL,
    usu_crea character varying(50) NOT NULL,
    fec_crea timestamp with time zone NOT NULL
);
 (   DROP TABLE public.sol_rela_pelis_actor;
       public         heap    postgres    false    202    3            �            1259    17760    sol_token_id_token_seq    SEQUENCE     �   CREATE SEQUENCE public.sol_token_id_token_seq
    START WITH 7
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 2147483647
    CACHE 1;
 -   DROP SEQUENCE public.sol_token_id_token_seq;
       public          postgres    false    3            �            1259    17762 	   sol_token    TABLE     C  CREATE TABLE public.sol_token (
    id_token integer DEFAULT nextval('public.sol_token_id_token_seq'::regclass) NOT NULL,
    token character varying(100) NOT NULL,
    id_usuario integer NOT NULL,
    vigente boolean NOT NULL,
    usu_crea character varying(50) NOT NULL,
    fec_crea timestamp with time zone NOT NULL
);
    DROP TABLE public.sol_token;
       public         heap    postgres    false    213    3            �            1259    17642    sol_usuario_id_usuario_seq    SEQUENCE     �   CREATE SEQUENCE public.sol_usuario_id_usuario_seq
    START WITH 7
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 2147483647
    CACHE 1;
 1   DROP SEQUENCE public.sol_usuario_id_usuario_seq;
       public          postgres    false    3            �            1259    17849    sol_usuario    TABLE     $  CREATE TABLE public.sol_usuario (
    id_usuario integer DEFAULT nextval('public.sol_usuario_id_usuario_seq'::regclass) NOT NULL,
    nombre character varying(100) NOT NULL,
    direccion character varying(20) NOT NULL,
    telefono character varying(20),
    tipo_usuario integer NOT NULL,
    rut character varying(100) NOT NULL,
    usuario character varying(20) NOT NULL,
    contrasena character varying(20) NOT NULL,
    vigente boolean NOT NULL,
    usu_crea character varying(50) NOT NULL,
    fec_crea timestamp with time zone NOT NULL
);
    DROP TABLE public.sol_usuario;
       public         heap    postgres    false    209    3            �            1259    17820    ta_auditoria_id_auditoria_seq    SEQUENCE     �   CREATE SEQUENCE public.ta_auditoria_id_auditoria_seq
    START WITH 7
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 2147483647
    CACHE 1;
 4   DROP SEQUENCE public.ta_auditoria_id_auditoria_seq;
       public          postgres    false    3            �            1259    17831    ta_auditoria    TABLE     �  CREATE TABLE public.ta_auditoria (
    id_auditoria integer DEFAULT nextval('public.ta_auditoria_id_auditoria_seq'::regclass) NOT NULL,
    codigoerror integer NOT NULL,
    msjerror character varying(1000),
    metodo character varying(50),
    classerror character varying(100),
    vigente boolean NOT NULL,
    usu_crea character varying(10),
    fec_crea timestamp with time zone NOT NULL
);
     DROP TABLE public.ta_auditoria;
       public         heap    postgres    false    218    3            t          0    17710 
   ma_actores 
   TABLE DATA           `   COPY public.ma_actores (id_actor, nombre, id_pelicula, vigente, usu_crea, fec_crea) FROM stdin;
    public          postgres    false    211   ��       s          0    17704    ma_directores 
   TABLE DATA           Y   COPY public.ma_directores (id_director, nombre, vigente, usu_crea, fec_crea) FROM stdin;
    public          postgres    false    210   �       �          0    25644 
   ma_estados 
   TABLE DATA           _   COPY public.ma_estados (id_estado, descripcion, tipo, vigente, usu_crea, fec_crea) FROM stdin;
    public          postgres    false    223   e�       y          0    17789    ma_tipo_usuario 
   TABLE DATA           \   COPY public.ma_tipo_usuario (id_tipo, descripcion, vigente, usu_crea, fec_crea) FROM stdin;
    public          postgres    false    216   �       ~          0    17855    sol_historico_pelis_orden 
   TABLE DATA           �   COPY public.sol_historico_pelis_orden (id_historico, id_orden, id_estado_actual, id_estado_anterior, vigente, usu_crea, fec_crea) FROM stdin;
    public          postgres    false    221   ]�       z          0    17801    sol_ornedes 
   TABLE DATA           �   COPY public.sol_ornedes (id_orden, codigo_orden, id_pelicula, id_usuario, num_prestadas, id_estado, vigente, usu_crea, fec_crea) FROM stdin;
    public          postgres    false    217   q�                 0    25630    sol_peliculas 
   TABLE DATA           �   COPY public.sol_peliculas (id_pelicula, nombre, descripcion, num_peliculas, num_disponibles, num_prestadas, id_director, id_estado, vigente, usu_crea, fec_crea) FROM stdin;
    public          postgres    false    222   �       u          0    17754    sol_rela_pelis_actor 
   TABLE DATA           o   COPY public.sol_rela_pelis_actor (id_relacion, id_pelicula, id_actor, vigente, usu_crea, fec_crea) FROM stdin;
    public          postgres    false    212   ��       w          0    17762 	   sol_token 
   TABLE DATA           ]   COPY public.sol_token (id_token, token, id_usuario, vigente, usu_crea, fec_crea) FROM stdin;
    public          postgres    false    214   �       }          0    17849    sol_usuario 
   TABLE DATA           �   COPY public.sol_usuario (id_usuario, nombre, direccion, telefono, tipo_usuario, rut, usuario, contrasena, vigente, usu_crea, fec_crea) FROM stdin;
    public          postgres    false    220   r�       |          0    17831    ta_auditoria 
   TABLE DATA           |   COPY public.ta_auditoria (id_auditoria, codigoerror, msjerror, metodo, classerror, vigente, usu_crea, fec_crea) FROM stdin;
    public          postgres    false    219   ��       �           0    0    ma_actores_id_actor_seq    SEQUENCE SET     F   SELECT pg_catalog.setval('public.ma_actores_id_actor_seq', 7, false);
          public          postgres    false    208            �           0    0    ma_directores_id_director_seq    SEQUENCE SET     L   SELECT pg_catalog.setval('public.ma_directores_id_director_seq', 7, false);
          public          postgres    false    207            �           0    0    ma_estados_id_estado_seq    SEQUENCE SET     G   SELECT pg_catalog.setval('public.ma_estados_id_estado_seq', 7, false);
          public          postgres    false    206            �           0    0    ma_tipo_usuario_id_tipo_seq    SEQUENCE SET     J   SELECT pg_catalog.setval('public.ma_tipo_usuario_id_tipo_seq', 7, false);
          public          postgres    false    215            �           0    0 *   sol_historico_pelis_orden_id_historico_seq    SEQUENCE SET     Y   SELECT pg_catalog.setval('public.sol_historico_pelis_orden_id_historico_seq', 23, true);
          public          postgres    false    205            �           0    0    sol_ornedes_id_orden_seq    SEQUENCE SET     G   SELECT pg_catalog.setval('public.sol_ornedes_id_orden_seq', 23, true);
          public          postgres    false    204            �           0    0    sol_peliculas_id_pelicula_seq    SEQUENCE SET     L   SELECT pg_catalog.setval('public.sol_peliculas_id_pelicula_seq', 7, false);
          public          postgres    false    203            �           0    0 (   sol_relacion_pelis_actor_id_relacion_seq    SEQUENCE SET     W   SELECT pg_catalog.setval('public.sol_relacion_pelis_actor_id_relacion_seq', 7, false);
          public          postgres    false    202            �           0    0    sol_token_id_token_seq    SEQUENCE SET     E   SELECT pg_catalog.setval('public.sol_token_id_token_seq', 66, true);
          public          postgres    false    213            �           0    0    sol_usuario_id_usuario_seq    SEQUENCE SET     H   SELECT pg_catalog.setval('public.sol_usuario_id_usuario_seq', 9, true);
          public          postgres    false    209            �           0    0    ta_auditoria_id_auditoria_seq    SEQUENCE SET     L   SELECT pg_catalog.setval('public.ta_auditoria_id_auditoria_seq', 64, true);
          public          postgres    false    218            �
           2606    17715    ma_actores ma_actores_pkey 
   CONSTRAINT     ^   ALTER TABLE ONLY public.ma_actores
    ADD CONSTRAINT ma_actores_pkey PRIMARY KEY (id_actor);
 D   ALTER TABLE ONLY public.ma_actores DROP CONSTRAINT ma_actores_pkey;
       public            postgres    false    211            �
           2606    17709     ma_directores ma_directores_pkey 
   CONSTRAINT     g   ALTER TABLE ONLY public.ma_directores
    ADD CONSTRAINT ma_directores_pkey PRIMARY KEY (id_director);
 J   ALTER TABLE ONLY public.ma_directores DROP CONSTRAINT ma_directores_pkey;
       public            postgres    false    210            �
           2606    25652    ma_estados ma_estados_pkey 
   CONSTRAINT     _   ALTER TABLE ONLY public.ma_estados
    ADD CONSTRAINT ma_estados_pkey PRIMARY KEY (id_estado);
 D   ALTER TABLE ONLY public.ma_estados DROP CONSTRAINT ma_estados_pkey;
       public            postgres    false    223            �
           2606    17794 $   ma_tipo_usuario ma_tipo_usuario_pkey 
   CONSTRAINT     g   ALTER TABLE ONLY public.ma_tipo_usuario
    ADD CONSTRAINT ma_tipo_usuario_pkey PRIMARY KEY (id_tipo);
 N   ALTER TABLE ONLY public.ma_tipo_usuario DROP CONSTRAINT ma_tipo_usuario_pkey;
       public            postgres    false    216            �
           2606    17860 8   sol_historico_pelis_orden sol_historico_pelis_orden_pkey 
   CONSTRAINT     �   ALTER TABLE ONLY public.sol_historico_pelis_orden
    ADD CONSTRAINT sol_historico_pelis_orden_pkey PRIMARY KEY (id_historico);
 b   ALTER TABLE ONLY public.sol_historico_pelis_orden DROP CONSTRAINT sol_historico_pelis_orden_pkey;
       public            postgres    false    221            �
           2606    17806    sol_ornedes sol_ornedes_pkey 
   CONSTRAINT     `   ALTER TABLE ONLY public.sol_ornedes
    ADD CONSTRAINT sol_ornedes_pkey PRIMARY KEY (id_orden);
 F   ALTER TABLE ONLY public.sol_ornedes DROP CONSTRAINT sol_ornedes_pkey;
       public            postgres    false    217            �
           2606    25638     sol_peliculas sol_peliculas_pkey 
   CONSTRAINT     g   ALTER TABLE ONLY public.sol_peliculas
    ADD CONSTRAINT sol_peliculas_pkey PRIMARY KEY (id_pelicula);
 J   ALTER TABLE ONLY public.sol_peliculas DROP CONSTRAINT sol_peliculas_pkey;
       public            postgres    false    222            �
           2606    17759 $   sol_rela_pelis_actor sol_pelis_actor 
   CONSTRAINT     k   ALTER TABLE ONLY public.sol_rela_pelis_actor
    ADD CONSTRAINT sol_pelis_actor PRIMARY KEY (id_relacion);
 N   ALTER TABLE ONLY public.sol_rela_pelis_actor DROP CONSTRAINT sol_pelis_actor;
       public            postgres    false    212            �
           2606    17767    sol_token sol_token_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY public.sol_token
    ADD CONSTRAINT sol_token_pkey PRIMARY KEY (id_token);
 B   ALTER TABLE ONLY public.sol_token DROP CONSTRAINT sol_token_pkey;
       public            postgres    false    214            �
           2606    17854    sol_usuario sol_usuario_pkey 
   CONSTRAINT     [   ALTER TABLE ONLY public.sol_usuario
    ADD CONSTRAINT sol_usuario_pkey PRIMARY KEY (rut);
 F   ALTER TABLE ONLY public.sol_usuario DROP CONSTRAINT sol_usuario_pkey;
       public            postgres    false    220            �
           2606    17839    ta_auditoria ta_auditoria_pkey 
   CONSTRAINT     f   ALTER TABLE ONLY public.ta_auditoria
    ADD CONSTRAINT ta_auditoria_pkey PRIMARY KEY (id_auditoria);
 H   ALTER TABLE ONLY public.ta_auditoria DROP CONSTRAINT ta_auditoria_pkey;
       public            postgres    false    219            t   T   x�3�tL.�/R(*MMJ�4�,�,�/.I/J-�4202�54�54R04�22�22�3677�0�50�2��L�/V(�����=... J�      s   F   x�3�t�,JM.�/
(*MMJ�,�,�/.I/J-�4202�54�54R04�22�20�33
���r��qqq ](      �   |   x���M�0���^����Z�l�y 7CB�����P��	sٷu~,��*�[��W.k-�H�K�&�<4{��ӿS`�d��MK��q	_�qH�ؖ�!�y���MƧ�ĵu7J�x�8�      y   \   x�3�t��L�+I�,�,�/.I/J-�4202�54�54R0��25�2��361150�50�2�tL����,.)JL�/«��T�������1F��� ���      ~     x����m1�3�
7`�Ï>,"�� v�W>G�Q�4���A/��~��~?���r�h7��HUn1�.qL�Wb�t�kz
U.�65���*{Hn�R�_�o"��2��רe�*f$�gS{��T���䮈%�*�gLnX�Q�B�4a���6�*;��H��C��eaS�~�ՍSˈ���'aS�2�.�� <�4*�`k������5GA���2u���=GI��<kpX��F�������jӾ����8� ����      z   �  x���Kn�0D��S�-���D]�/0��,'Hr�$i� ��^���E�o7��
���Z#�[��c{��������6eW����6<�j�ؐW�K��RJ����
�Vu���(����)��B��2�Bn����O�âPi��SM��7s��[;E���6���k���DuHV�;)����mO�I�;I��Hׇ�4�P�Z�Y���ⴍ�B-�n�v�tcy�W��!��Bq˝Զ[~�h�z��F�)Ib�I��$��T�醵�)
�	Rա��!�o��p>�\=@�p�R�(��� 5�-WrN�Y�&��b�R�x������b�B��nI�Ґ�GX�=�sR�e
��V��c���Z��\���)�ץP�m5�W�\.�ˁ�q         �   x���1�0��zs�\���,LBj;;Č�B�<�G�b������/>K�L�8�%�>}?�~D�$�[�r3�q��%NdaQ1W�48��ط�+4J�\�W7�ȴ��XXҚZЬ,ӥ��۱y���L���A�jތR���B�      u   W   x�u�1� FṜ�@��"г�G�p��;�_�A�����8q
@ {c�J�VEkH��ļ�ۊ�Ͻ���X�M����{r�      w   K  x���Mnd7���S��OIg�6�2Af���~
0OY�7.��bQ�/6!!f���������_����߿�x������Ǥ��2��E��_��7Bù���ӹA&�1��02���D0�������_ >;+%�#@hr��*���8�8��)� ����os%�#¦�*�t!�.��{�! �����}zo�9�b�?���p��ň�� Á���v�_앿D�'�4��d���F�����#"�Щ� �ߧ��[���(([�l�2Ü�f$1�ψ���p����1�z�R��v:FVO8dī���ӛ1<T�WB��9E`ť�ok�0�%2<O���"�T�q��Q�Az���eq�?7D|Dµ��Ti%��UA�6�(*��B�z�̮1��D�m}�g!�:R�eO��#`�yz��1��Y���{9���s-�+{�u�~�o1�F�g!p�Ѡ
[�'c��"��+���[��Xdz�D�>F����OB~�a-�X��|�	�͸�?c!�W5�ұ,����$��?�B���d�%v���"�?�d]�\1~[+�����������5�&o'�>���������t�Զ- �bY������Wf��g�A�?��E�b�ٸ�Z�!`lL������
g��^ ���S��Z���.S!��殧Iװ���1�*����PDM�<��B�؆�^�i�=�nT����9��WG���D��F=�11 �Ho�΋q�Pp7��S)����.�r��z"�G����J����):zZ;�t �U�?|�/�nͮ"�ǅ�H�U���~�c�0��h%�en��H*�A�x3dM�
�\��h���r=Y��1d�}�ݙ�8	<8�B�d�L�x������<����ԮzbL|���p�B�0O|YX�؂����׳�z��d�n�]	̦8�x���!U���
��-(�����8!PKB)paZ��-Al2�iL!�>xmb��ܗ)·��d��r�Nk��5w� ����r>�D��MA��6����� -c�5      }     x���=k�0���+�et�;��溁����v�&��$������R�~,����<z��q<�oA�L�0n{h�n�*d5S��
��i�E	�)v9�5`����N`�5Q�S(se˜�4H�H�Po��n;ư�Gu~ �O�M���N��4qL`�2��<���Y�}?��	���������h%N�L�h�K�G̬S�w������b��{gZ���vQ����|Y���f���ES�t�Y�>uB�l,��$$����Ƶ�^aQU\��9���<˲/cv      |   H  x��\�n�H]+_QKP8�f�@P�L"@��zt0��,�mb(RMR���M�����.���U����X���!9¢����h��kQ���(͐��A�Fǳt��7E��a���|��g:M�g����\�K��3���'u��91����A��{����+����2)2	�}r��;*�iV 8���ĤeVxhf�L�J��:�3�Ors�s�(=3y�&5����_��v7�ge�ϊ���4DDԎ۠ɇ�,6�� �=_�aQ+k��&.jS����D"BCFBx3��k,^�-n��FC*=P���-��*n�x�� �-no�[8R���B8���Հ�!���1��G����	����*��p*d�S&y��p�X��(��F`�� ���܀>i;g�l�~�3>�@����HO㓙�R�R�I����g��$נ7Letc}��������sf�,>\����.�N㏧ Px	�ŗWbrA�GK�ئF#e��h0�u��è���A��Z����ruH(p�=5@+����R�k�)�]��L8������ap2����jq�-�ASB�h���̧MJ�f�J�ĠN��O����%:�f�xT���b��}Sz��c�g�ǎO�� ~H .�p�]$�&����,�dT��ԙ�b�e��W$�]}7��q߰������B�J�eQ�KԜOJH�4_��0�Y���l�Y���8��ljr0F�%
�"dDo�ѯG���K/'��Q�R��h����Y1��B���,�zص�Y��~Av`����!`�,���<p�X��1�0񕢎b����Q�
,���(�[�_ŶXL<��/]`F�-�/�br�a,$��b����Q���1Q.���z>�&�~l.�ר �?�lV7�C�ZJi�9,|OBƧ\�-��l$�P�G8e�%b�����	œ�K������3��OI�}gK��MZ�	$�zRQ\��ز��A�V	��F	�6!��*?p�ȍ����1:����-$�PϗT�ʼ.�ִA����=�D ]%�m�W���B	�*���~:�O �27�O�b-�P�J���e���b�]v�/�VH�m{ͦ��:�!wE�z�.���_=EL~� @WN)��ª�T Ij��h�\�ڮg(���$�g����[��g�����3�H��&U.y���
� �~~�pb�q��SeHO��'f��,���$Y�߈%xH !���]+���VtfI�� ��F��ck4�Y�%��-E���
.���\�_�5:���_g��M���I���~Y�}���:Z�_G7�\*fC�ݫl]�'�?���p5�h���G�ju]�?���jz�n{���t{�fw��[�Q��uv҃��W�������5��a�}�Lg�Z�^��z������ًu�6j��];�/��0A���rjw�����]L?���w�wvw�&��yg�P����v���~�]����`1��V3
&2f�Q+.5��jF\�X����R���ۻC(��\��
2u�d�o��f�ю��h���n��R����Q/B�[���+jt�侱,���ގ����X�IZ�9�4���#�v\n��j�G<�۷`ݟ��`>��4G跸<E:��#>�xrg�΀L�_�ޙr��ۤ}c5�|��s�>�/9W���-�`��#(%��H>�D�e�X)��������W�U�]"RXG�M��D��JDC̬D�AU?��%�@�G�Tԭl��f�E"�_x����]������k	�3�+e)>"�	c�b����Y����C��.$�EP�t��K[�
�������$�(X|�����M�5��c���`���{�	ћ��{��D�!��1	��g������=	��QA�p[a�ݻ��xb�%�i��!�O����9�PI��s	�U�����G9֠�+� ��(��7&��Lta=��o���19�+,�ߑ)!�7x��m
�wU[[]{�l�̧J���}b�I�*�xô'ؾ�ɩ�^�M�~���_7�t����q��ߥ�l���!��Ӱ�{K�Q�C��{��w��{����Ϳ��     
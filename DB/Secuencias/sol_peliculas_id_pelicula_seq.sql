-- SEQUENCE: public.sol_peliculas_id_pelicula_seq

-- DROP SEQUENCE public.sol_peliculas_id_pelicula_seq;

CREATE SEQUENCE public.sol_peliculas_id_pelicula_seq
    INCREMENT 1
    START 7
    MINVALUE 1
    MAXVALUE 2147483647
    CACHE 1;

ALTER SEQUENCE public.sol_peliculas_id_pelicula_seq
    OWNER TO postgres;
-- SEQUENCE: public.ma_estados_id_estado_seq

-- DROP SEQUENCE public.ma_estados_id_estado_seq;

CREATE SEQUENCE public.ma_estados_id_estado_seq
    INCREMENT 1
    START 7
    MINVALUE 1
    MAXVALUE 2147483647
    CACHE 1;

ALTER SEQUENCE public.ma_estados_id_estado_seq
    OWNER TO postgres;
-- SEQUENCE: public.sol_historico_pelis_orden_id_historico_seq

-- DROP SEQUENCE public.sol_historico_pelis_orden_id_historico_seq;

CREATE SEQUENCE public.sol_historico_pelis_orden_id_historico_seq
    INCREMENT 1
    START 7
    MINVALUE 1
    MAXVALUE 2147483647
    CACHE 1;

ALTER SEQUENCE public.sol_historico_pelis_orden_id_historico_seq
    OWNER TO postgres;
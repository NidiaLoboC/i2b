-- FUNCTION: public.fr_listar_director(character varying, integer, integer)

-- DROP FUNCTION public.fr_listar_director(character varying, integer, integer);

CREATE OR REPLACE FUNCTION public.fr_listar_director(
	p_token character varying,
	p_id_usuario integer,
	p_id_director integer)
	RETURNS TABLE(ID_DIRECTOR INTEGER, NOMBRE CHARACTER VARYING) AS $$
	DECLARE
	
BEGIN

--	IF EXISTS(SELECT ST.ID_TOKEN FROM SOL_TOKEN ST WHERE ST.TOKEN=P_TOKEN AND ST.ID_USUARIO=P_ID_USUARIO AND ST.VIGENTE=TRUE) THEN
		
		RETURN QUERY SELECT MD.ID_DIRECTOR,MD.NOMBRE
					FROM MA_DIRECTORES MD
					WHERE MD.VIGENTE=TRUE AND
					(P_ID_DIRECTOR =0 OR MD.ID_DIRECTOR= P_ID_DIRECTOR) ;   -- OPEN A CURSOR
			  	
--	END IF;
	
	END;
$$ LANGUAGE plpgsql;

ALTER FUNCTION public.fr_listar_director(character varying, integer, integer)
	OWNER TO postgres;

-- FUNCTION: public.fr_historico_ordenes(integer, integer, integer)

-- DROP FUNCTION public.fr_historico_ordenes(integer, integer, integer;

CREATE OR REPLACE FUNCTION public.fr_historico_ordenes(
	p_id_orden integer,
	p_id_estado_actual integer,
	p_id_estado_anterior integer,
	OUT P_id_historico integer 
	)
    RETURNS integer
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    
AS $BODY$DECLARE
	
BEGIN

	WITH UPD AS (INSERT INTO PUBLIC.SOL_HISTORICO_PELIS_ORDEN(
	ID_ORDEN, ID_ESTADO_ACTUAL, ID_ESTADO_ANTERIOR, VIGENTE, USU_CREA, FEC_CREA)
	VALUES ( P_ID_ORDEN, P_ID_ESTADO_ACTUAL, P_ID_ESTADO_ANTERIOR, TRUE, USER, NOW()) RETURNING ID_HISTORICO )
	SELECT INTO P_ID_HISTORICO ID_HISTORICO FROM UPD;
	
END$BODY$;

ALTER FUNCTION public.fr_historico_ordenes(integer, integer, integer)
	OWNER TO postgres;

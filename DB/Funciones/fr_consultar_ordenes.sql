-- FUNCTION: public.fr_consultar_ordenes(integer, integer, integer,character varying,character varying)

-- DROP FUNCTION public.fr_consultar_ordenes(integer, integer, integer,character varying,character varying)

CREATE OR REPLACE FUNCTION public.fr_consultar_ordenes(
	p_token character varying,
	p_id_usuario integer,
	p_id_orden character varying,
	p_id_estado integer,
	p_fecha_inicio character varying,
	p_fecha_fin character varying)
	RETURNS TABLE(ID_ORDEN INTEGER,ORDEN CHARACTER VARYING,PELICULA CHARACTER VARYING,USUARIO CHARACTER VARYING,ESTADO CHARACTER VARYING) AS $$
	DECLARE
	
BEGIN

	IF EXISTS(SELECT ST.ID_TOKEN FROM SOL_TOKEN ST WHERE ST.TOKEN=P_TOKEN AND ST.ID_USUARIO=P_ID_USUARIO AND ST.VIGENTE=TRUE) THEN
		
		RETURN QUERY SELECT SO.ID_ORDEN,SO.CODIGO_ORDEN AS ORDEN,SP.NOMBRE AS PELICULA,SU.NOMBRE AS USUARIO,ME.DESCRIPCION AS ESTADO
						FROM SOL_ORNEDES AS SO
						LEFT JOIN SOL_PELICULAS SP ON SP.ID_PELICULA=SO.ID_PELICULA
						LEFT JOIN MA_ESTADOS ME ON ME.ID_ESTADO=SO.ID_ESTADO
						LEFT JOIN SOL_USUARIO SU ON SU.ID_USUARIO=SO.ID_USUARIO
						WHERE SO.VIGENTE=TRUE AND 
						(P_ID_USUARIO =0 OR SU.ID_USUARIO=P_ID_USUARIO) AND
						(P_ID_ORDEN IS NULL OR SO.CODIGO_ORDEN=P_ID_ORDEN) AND
						(P_FECHA_INICIO IS NULL OR SO.FEC_CREA >= TO_TIMESTAMP(P_FECHA_INICIO||'00:0', 'DD-MM-YYYY SS:MS') OR SO.FEC_CREA <=  TO_TIMESTAMP(P_FECHA_FIN||'00:0', 'DD-MM-YYYY SS:MS') ) AND
						(P_ID_ESTADO =0 OR SO.ID_ESTADO=P_ID_ESTADO);   -- OPEN A CURSOR
			  	
	END IF;
	
	END;
$$ LANGUAGE plpgsql;

ALTER FUNCTION public.fr_consultar_ordenes(character varying, integer,character varying, integer,character varying,character varying)
	OWNER TO postgres;

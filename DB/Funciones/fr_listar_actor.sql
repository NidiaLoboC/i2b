-- FUNCTION: public.fr_listar_estado(character varying, integer, integer, integer)

-- DROP FUNCTION public.fr_listar_estado(character varying, integer, integer, integer);

CREATE OR REPLACE FUNCTION public.fr_listar_actor(
	p_token character varying,
	p_id_usuario integer,
	p_id_pelicula integer,
	p_id_actores integer)
	RETURNS TABLE(ID_ACTOR INTEGER, NOMBRE CHARACTER VARYING) AS $$
	DECLARE
	
BEGIN

	--IF EXISTS(SELECT ST.ID_TOKEN FROM SOL_TOKEN ST WHERE ST.TOKEN=P_TOKEN AND ST.ID_USUARIO=P_ID_USUARIO AND ST.VIGENTE=TRUE) THEN
		
		RETURN QUERY SELECT MA.ID_ACTOR,MA.NOMBRE
					FROM MA_ACTORES MA INNER JOIN SOL_RELA_PELIS_ACTOR SRPA ON SRPA.ID_ACTOR=MA.ID_ACTOR
					INNER JOIN SOL_PELICULAS SP ON SP.ID_PELICULA=SRPA.ID_PELICULA AND SRPA.VIGENTE=TRUE
					WHERE (P_ID_PELICULA =0 OR SRPA.ID_PELICULA=P_ID_PELICULA) AND
					(P_ID_ACTORES =0 OR MA.ID_ACTOR=P_ID_ACTORES) ;   -- OPEN A CURSOR
			  	
--	END IF;
	
	END;
$$ LANGUAGE plpgsql;

ALTER FUNCTION public.fr_listar_actor(character varying, integer, integer, integer)
	OWNER TO postgres;

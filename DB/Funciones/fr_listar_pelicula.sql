-- FUNCTION: public.fr_listar_pelicula(integer, integer, integer)

-- DROP FUNCTION public.fr_listar_pelicula(integer, integer, integer);

CREATE OR REPLACE FUNCTION public.fr_listar_pelicula(
	p_token character varying,
	p_id_usuario integer,
	p_id_pelicula integer)
	RETURNS TABLE(ID_PELICULA INTEGER, NOMBRE CHARACTER VARYING) AS $$
	DECLARE
	
BEGIN

	--IF EXISTS(SELECT ST.ID_TOKEN FROM SOL_TOKEN ST WHERE ST.TOKEN=P_TOKEN AND ST.ID_USUARIO=P_ID_USUARIO AND ST.VIGENTE=TRUE) THEN
		
		RETURN QUERY SELECT SP.ID_PELICULA, SP.NOMBRE
						FROM SOL_PELICULAS SP
						WHERE SP.VIGENTE=TRUE AND 
						(P_ID_PELICULA=0 OR SP.ID_PELICULA=P_ID_PELICULA);   -- OPEN A CURSOR
			  	
--	END IF;
	
	END;
$$ LANGUAGE plpgsql;

ALTER FUNCTION public.fr_listar_pelicula(character varying, integer, integer)
	OWNER TO postgres;

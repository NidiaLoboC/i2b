-- FUNCTION: public.fr_listar_estado(character varying, integer, integer)

-- DROP FUNCTION public.fr_listar_estado(character varying, integer, integer);

CREATE OR REPLACE FUNCTION public.fr_listar_estado(
	p_token character varying,
	p_id_usuario integer,
	p_id_estado integer)
	RETURNS TABLE(ID_ESTADO INTEGER, DESCRIPCION CHARACTER VARYING) AS $$
	DECLARE
	
BEGIN

	IF EXISTS(SELECT ST.ID_TOKEN FROM SOL_TOKEN ST WHERE ST.TOKEN=P_TOKEN AND ST.ID_USUARIO=P_ID_USUARIO AND ST.VIGENTE=TRUE) THEN
		
		RETURN QUERY SELECT MA.ID_ESTADO,MA.DESCRIPCION
					FROM MA_ESTADOS MA
					WHERE  MA.VIGENTE=TRUE AND MA.TIPO=2 AND 
					(P_ID_ESTADO =0 OR MA.ID_ESTADO= P_ID_ESTADO) ;   -- OPEN A CURSOR --TIPO 2 ES ORDEN TIPO 1 PELICULA
			  	
	END IF;
	
	END;
$$ LANGUAGE plpgsql;

ALTER FUNCTION public.fr_listar_estado(character varying, integer, integer)
	OWNER TO postgres;

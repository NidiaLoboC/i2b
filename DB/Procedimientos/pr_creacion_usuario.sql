-- PROCEDURE: public.pr_login(character varying, character varying, integer, character varying, character varying, boolean)

-- DROP PROCEDURE public.pr_creacion_usuario(character varying, character varying,character varying, character varying, integer, character varying, character varying, integer, character varying, boolean);

CREATE OR REPLACE PROCEDURE public.pr_creacion_usuario(
	p_rut character varying,
	p_nombre character varying,
	p_direccion character varying,
	p_telefono character varying,
	p_id_tipo_usuario integer,
	p_user character varying,
	p_pass character varying,
	INOUT p_id_usuario integer,
	INOUT p_message_out character varying,
	INOUT p_eserror_out boolean)
LANGUAGE 'plpgsql'

AS $BODY$DECLARE
  --V_CURSOR refcursor;                                
  -- Declare a cursor variable  
BEGIN
	
	P_ESERROR_OUT := false;
	P_MESSAGE_OUT := 'Creación de usuario exitoso.';
	
	IF EXISTS(SELECT ID_USUARIO FROM SOL_USUARIO WHERE RUT=P_RUT AND VIGENTE=TRUE) THEN
  	
		P_ESERROR_OUT := TRUE;
		P_MESSAGE_OUT := 'Este usuario ya está creado.'; 
				
	ELSE
	
		WITH UPD AS (INSERT INTO PUBLIC.SOL_USUARIO(NOMBRE, DIRECCION, TELEFONO, TIPO_USUARIO, RUT, USUARIO,
			CONTRASENA, VIGENTE, USU_CREA, FEC_CREA)
			VALUES (P_NOMBRE, 
			P_DIRECCION, 
			P_TELEFONO,
			P_ID_TIPO_USUARIO, 
			P_RUT, 
			P_USER, 
			P_PASS,
			TRUE, 
			USER,
			NOW()) RETURNING ID_USUARIO)
			SELECT INTO P_ID_USUARIO ID_USUARIO FROM UPD;
			
			IF P_ID_USUARIO IS NULL THEN	
			P_ESERROR_OUT := TRUE;
			P_MESSAGE_OUT := 'No fue posible crear el usuario.';	
	 		
			END IF;
		
	END IF;
		
	--RETURN 0;
	
END$BODY$;
ALTER PROCEDURE public.pr_creacion_usuario(character varying, character varying,character varying,
 character varying, integer, character varying, character varying, integer, character varying, boolean)
    OWNER TO postgres;

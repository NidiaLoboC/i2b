-- PROCEDURE: public.pr_auditoria(integer, character varying, character varying, character varying, character varying, boolean)

-- DROP PROCEDURE public.pr_auditoria(integer, character varying, character varying, character varying, character varying, boolean)

CREATE OR REPLACE PROCEDURE public.pr_auditoria(
	p_errorCode integer,
	p_msjError character varying,
	p_methodError character varying,
	p_classError character varying,
	INOUT p_message_out character varying,
	INOUT p_eserror_out boolean)
LANGUAGE 'plpgsql'

AS $BODY$DECLARE
  --V_CURSOR refcursor;                                
  -- Declare a cursor variable  
BEGIN
	
	P_ESERROR_OUT := false;
	P_MESSAGE_OUT := 'Registro guardado con exitoso.';
	
	INSERT INTO PUBLIC.TA_AUDITORIA(
	 CODIGOERROR, MSJERROR, METODO,CLASSERROR, VIGENTE, USU_CREA, FEC_CREA)
	VALUES (P_ERRORCODE, P_MSJERROR, P_METHODERROR, P_CLASSERROR, TRUE, USER, NOW());
	
END$BODY$;
ALTER PROCEDURE public.pr_auditoria(integer, character varying, character varying, character varying, character varying, boolean)
    OWNER TO postgres;

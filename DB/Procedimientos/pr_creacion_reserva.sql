-- PROCEDURE: public.pr_creacion_reserva(character varying, integer, integer, integer, integer, integer, character varying, boolean)

-- DROP PROCEDURE public.pr_creacion_reserva(character varying, integer, integer, integer, integer, integer, character varying, boolean);

CREATE OR REPLACE PROCEDURE public.pr_creacion_reserva(
	p_token character varying,
	p_id_usuario integer,
	p_id_pelicula integer,
	p_numero_reserva integer,
	p_id_estado integer,
	inout p_id_orden integer,
	inout codorden character varying,
	inout p_message_out character varying,
	inout p_eserror_out boolean)
LANGUAGE 'plpgsql'

AS $BODY$DECLARE
  --V_CURSOR refcursor;                                
  -- Declare a cursor variable  
  RESULTADO INTEGER:= 0;
BEGIN
	
	P_ESERROR_OUT := false;
	P_MESSAGE_OUT := 'Creación de reserva con exito.';
	
	
	IF EXISTS(SELECT ST.ID_TOKEN FROM SOL_TOKEN ST WHERE ST.TOKEN=P_TOKEN AND ST.ID_USUARIO=P_ID_USUARIO AND ST.VIGENTE=TRUE) THEN
	
		IF EXISTS(SELECT NUM_DISPONIBLES FROM SOL_PELICULAS WHERE VIGENTE=TRUE AND NUM_DISPONIBLES<=NUM_PELICULAS AND 
				  NUM_DISPONIBLES>0 AND ID_PELICULA=P_ID_PELICULA) THEN
	
			IF EXISTS(SELECT NUM_DISPONIBLES FROM SOL_PELICULAS WHERE VIGENTE=TRUE AND NUM_DISPONIBLES<=NUM_PELICULAS AND 
					  NUM_DISPONIBLES>0 AND NUM_DISPONIBLES>=P_NUMERO_RESERVA AND ID_PELICULA=P_ID_PELICULA) THEN
					  
					  
			UPDATE PUBLIC.SOL_PELICULAS SP
				SET NUM_DISPONIBLES=(SELECT (SPU.NUM_DISPONIBLES-P_NUMERO_RESERVA) FROM SOL_PELICULAS SPU WHERE SPU.ID_PELICULA=P_ID_PELICULA), 
				NUM_PRESTADAS=(SELECT (SPU.NUM_PRESTADAS+P_NUMERO_RESERVA) FROM SOL_PELICULAS SPU WHERE SPU.ID_PELICULA=P_ID_PELICULA),
				ID_ESTADO=(SELECT CASE WHEN (SPU.NUM_DISPONIBLES-P_NUMERO_RESERVA)=0 THEN 2 ELSE 1 END FROM SOL_PELICULAS SPU WHERE SPU.ID_PELICULA=P_ID_PELICULA)
				WHERE SP.ID_PELICULA=P_ID_PELICULA;
				
				CODORDEN:=(SELECT 'O'||FLOOR(RANDOM()* (11) + 5)||''|| TO_CHAR(NOW() , 'YYYYMMDDHH24MISSMS')  ||''||FLOOR(RANDOM()* (11) + 5));
		
			WITH UPD AS (INSERT INTO PUBLIC.SOL_ORNEDES(
				CODIGO_ORDEN, ID_PELICULA, ID_USUARIO, NUM_PRESTADAS, ID_ESTADO, VIGENTE,USU_CREA, FEC_CREA)
				VALUES (CODORDEN, 
				P_ID_PELICULA, 
				P_ID_USUARIO,
				P_NUMERO_RESERVA,
				P_ID_ESTADO, 
				TRUE, 
				USER,
				NOW()) RETURNING ID_ORDEN )
				SELECT INTO P_ID_ORDEN ID_ORDEN FROM UPD;
				
				IF P_id_orden IS NULL THEN	
				P_ESERROR_OUT := TRUE;
				P_MESSAGE_OUT := 'No fue posible crear la orden.';	
				
				else
				
				RESULTADO:= (SELECT FR_HISTORICO_ORDENES(P_ID_ORDEN,P_ID_ESTADO,
				(SELECT COALESCE(ID_ESTADO_ANTERIOR,0) FROM SOL_HISTORICO_PELIS_ORDEN WHERE ID_ORDEN=P_ID_ORDEN AND VIGENTE=TRUE)));
				
				END IF;
					
			ELSE
			
				P_ESERROR_OUT := TRUE;
				P_MESSAGE_OUT := 'Cantidades de unidiades a prestar supera el número disponible.'; 
				
			END IF;
				
		ELSE
		
			P_ESERROR_OUT := TRUE;
			P_MESSAGE_OUT := 'La pelicula que intenta reservar no cuenta con unidades disponibles, intente nuevamente en unos minutos.'; 
			
		END IF;
		
	ELSE
	
		P_ESERROR_OUT := TRUE;
		P_MESSAGE_OUT := 'Este usuario ya está creado.'; 
		
	END IF;
		
	--RETURN 0;
	
END$BODY$;
ALTER PROCEDURE public.pr_creacion_reserva(character varying, integer, integer, integer, integer, integer, character varying, character varying, boolean)
    OWNER TO postgres;

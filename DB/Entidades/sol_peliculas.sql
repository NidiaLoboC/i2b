-- Table: public.sol_peliculas

-- DROP TABLE public.sol_peliculas;

CREATE TABLE public.sol_peliculas
(
    id_pelicula integer NOT NULL DEFAULT nextval('sol_peliculas_id_pelicula_seq'::regclass),
    nombre character varying(20) COLLATE pg_catalog."default" NOT NULL,
    descripcion character varying(1000) COLLATE pg_catalog."default" NOT NULL,
    num_peliculas integer NOT NULL,
	num_disponibles integer,
	num_prestadas integer,
	id_director integer NOT NULL,
	id_estado integer NOT NULL,
	vigente boolean NOT NULL,
    usu_crea character varying(50) COLLATE pg_catalog."default" NOT NULL,
    fec_crea timestamp with time zone NOT NULL,
    CONSTRAINT sol_peliculas_pkey PRIMARY KEY (id_pelicula)
)

TABLESPACE pg_default;

ALTER TABLE public.sol_peliculas
    OWNER to postgres;
-- Table: public.ta_auditoria

-- DROP TABLE public.ta_auditoria;

CREATE TABLE public.ta_auditoria
(
    id_auditoria integer NOT NULL DEFAULT nextval('ta_auditoria_id_auditoria_seq'::regclass),
    codigoerror integer NOT NULL,
    msjerror character varying(1000),
    metodo character varying(50),
    classerror character varying(100),
    vigente boolean NOT NULL,
    usu_crea character varying(10),
    fec_crea timestamp with time zone NOT NULL,
    CONSTRAINT ta_auditoria_pkey PRIMARY KEY (id_auditoria)
)

TABLESPACE pg_default;

ALTER TABLE public.ta_auditoria
    OWNER to postgres;
-- Table: public.ma_estados

-- DROP TABLE public.ma_estados;

CREATE TABLE public.ma_estados
(
    id_estado integer NOT NULL DEFAULT nextval('ma_estados_id_estado_seq'::regclass),
    descripcion character varying(500) COLLATE pg_catalog."default" NOT NULL,
    tipo integer NOT NULL,
    vigente boolean NOT NULL,
    usu_crea character varying(50) COLLATE pg_catalog."default" NOT NULL,
    fec_crea timestamp with time zone NOT NULL,
    CONSTRAINT ma_estados_pkey PRIMARY KEY (id_estado)
)

TABLESPACE pg_default;

ALTER TABLE public.ma_estados
    OWNER to postgres;
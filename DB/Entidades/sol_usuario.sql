-- Table: public.sol_usuario

-- DROP TABLE public.sol_usuario;

CREATE TABLE public.sol_usuario
(
    id_usuario integer NOT NULL DEFAULT nextval('sol_usuario_id_usuario_seq'::regclass),
    nombre character varying(100) COLLATE pg_catalog."default" NOT NULL,
    direccion character varying(20) COLLATE pg_catalog."default" NOT NULL,
    telefono character varying(20),
    tipo_usuario integer NOT NULL,
    rut character varying(100) NOT NULL,
    usuario character varying(20) COLLATE pg_catalog."default" NOT NULL,
    contrasena character varying(20) COLLATE pg_catalog."default" NOT NULL,
    vigente boolean NOT NULL,
    usu_crea character varying(50) COLLATE pg_catalog."default" NOT NULL,
    fec_crea timestamp with time zone NOT NULL,
    CONSTRAINT sol_usuario_pkey PRIMARY KEY (rut)
)

TABLESPACE pg_default;

ALTER TABLE public.sol_usuario
    OWNER to postgres;
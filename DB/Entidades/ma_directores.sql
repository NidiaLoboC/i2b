-- Table: public.ma_directores

-- DROP TABLE public.ma_directores;

CREATE TABLE public.ma_directores
(
    id_director integer NOT NULL DEFAULT nextval('ma_directores_id_director_seq'::regclass),
    nombre character varying(20) COLLATE pg_catalog."default" NOT NULL,
    vigente boolean NOT NULL,
    usu_crea character varying(50) COLLATE pg_catalog."default" NOT NULL,
    fec_crea timestamp with time zone NOT NULL,
    CONSTRAINT ma_directores_pkey PRIMARY KEY (id_director)
)

TABLESPACE pg_default;

ALTER TABLE public.ma_directores
    OWNER to postgres;
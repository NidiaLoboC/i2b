-- Table: public.sol_historico_pelis_orden

-- DROP TABLE public.sol_historico_pelis_orden;

CREATE TABLE public.sol_historico_pelis_orden
(
    id_historico integer NOT NULL DEFAULT nextval('sol_historico_pelis_orden_id_historico_seq'::regclass),
    id_orden integer NOT NULL,
    id_estado_actual integer NOT NULL,
	id_estado_anterior integer,
	vigente boolean NOT NULL,
    usu_crea character varying(50) COLLATE pg_catalog."default" NOT NULL,
    fec_crea timestamp with time zone NOT NULL,
    CONSTRAINT sol_historico_pelis_orden_pkey PRIMARY KEY (id_historico)
)

TABLESPACE pg_default;

ALTER TABLE public.sol_historico_pelis_orden
    OWNER to postgres;
	
	
	

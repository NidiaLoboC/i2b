-- Table: public.sol_ornedes

-- DROP TABLE public.sol_ornedes;

CREATE TABLE public.sol_ornedes
(
    id_orden integer NOT NULL DEFAULT nextval('sol_ornedes_id_orden_seq'::regclass),
    codigo_orden character varying(30) COLLATE pg_catalog."default" NOT NULL,
    id_pelicula integer NOT NULL,
	id_usuario integer NOT NULL,
	num_prestadas integer NOT NULL,
	id_estado integer NOT NULL,
	vigente boolean NOT NULL,
    usu_crea character varying(50) COLLATE pg_catalog."default" NOT NULL,
    fec_crea timestamp with time zone NOT NULL,
    CONSTRAINT sol_ornedes_pkey PRIMARY KEY (id_orden)
)

TABLESPACE pg_default;

ALTER TABLE public.sol_ornedes
    OWNER to postgres;
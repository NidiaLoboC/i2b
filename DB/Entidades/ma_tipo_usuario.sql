-- Table: public.ma_tipo_usuario

-- DROP TABLE public.ma_tipo_usuario;

CREATE TABLE public.ma_tipo_usuario
(
    id_tipo integer NOT NULL DEFAULT nextval('ma_tipo_usuario_id_tipo_seq'::regclass),
    descripcion character varying(20) COLLATE pg_catalog."default" NOT NULL,
    vigente boolean NOT NULL,
    usu_crea character varying(50) COLLATE pg_catalog."default" NOT NULL,
    fec_crea timestamp with time zone NOT NULL,
    CONSTRAINT ma_tipo_usuario_pkey PRIMARY KEY (id_tipo)
)

TABLESPACE pg_default;

ALTER TABLE public.ma_tipo_usuario
    OWNER to postgres;
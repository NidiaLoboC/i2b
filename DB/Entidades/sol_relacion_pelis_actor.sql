-- Table: public.sol_relacion_pelis_actor

-- DROP TABLE public.sol_relacion_pelis_actor;

CREATE TABLE public.sol_relacion_pelis_actor
(
    id_relacion integer NOT NULL DEFAULT nextval('sol_relacion_pelis_actor_id_relacion_seq'::regclass),
    id_pelicula integer NOT NULL,
	id_actor integer NOT NULL,
	vigente boolean NOT NULL,
    usu_crea character varying(50) COLLATE pg_catalog."default" NOT NULL,
    fec_crea timestamp with time zone NOT NULL,
    CONSTRAINT sol_pelis_actor PRIMARY KEY (id_relacion)
)

TABLESPACE pg_default;

ALTER TABLE public.sol_relacion_pelis_actor
    OWNER to postgres;
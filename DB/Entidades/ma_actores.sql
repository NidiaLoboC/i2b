-- Table: public.ma_actores

-- DROP TABLE public.ma_actores;

CREATE TABLE public.ma_actores
(
    id_actor integer NOT NULL DEFAULT nextval('ma_actores_id_actor_seq'::regclass),
    nombre character varying(20) COLLATE pg_catalog."default" NOT NULL,
    id_pelicula integer NOT NULL,
	vigente boolean NOT NULL,
    usu_crea character varying(50) COLLATE pg_catalog."default" NOT NULL,
    fec_crea timestamp with time zone NOT NULL,
    CONSTRAINT ma_actores_pkey PRIMARY KEY (id_actor)
)

TABLESPACE pg_default;

ALTER TABLE public.ma_actores
    OWNER to postgres;
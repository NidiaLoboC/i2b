/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com;

import com.manage.Controller;
import com.model.BeanActor;
import com.model.BeanDirector;
import com.model.BeanUser;
import com.model.BeanSearchOrder;
import com.model.BeanMovies;
import com.model.BeanOrder;
import com.model.BeanOrderDetail;
import com.model.BeanSelectMovies;
import com.model.BeanSession;
import com.model.BeanState;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;

/**
 * REST Web Service
 *
 * @author 81001587
 */
@Path("")
public class WsI2b {

    private Controller controll = new Controller();

    public WsI2b() {
    }

    @POST
    @Path("movie/login")
    @Produces("application/json")
    public Response MovieLogin(@FormParam("user") @DefaultValue("") String user,
            @FormParam("pass") @DefaultValue("") String pass) {
        if (user.length() > 0 && pass.length() > 0) {
            BeanSession bs = new BeanSession(user, pass);

            return Response.ok(controll.GetSession(bs))
                    .header("Access-Control-Allow-Origin", "*")
                    .header("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE")
                    .header("Access-Control-Max-Age", "3600")
                    .header("Access-Control-Allow-Headers", "x-requested-with").build();
        } else {
            return Response.status(Response.Status.BAD_REQUEST).header("Access-Control-Allow-Origin", "*")
                    .header("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE")
                    .header("Access-Control-Max-Age", "3600")
                    .header("Access-Control-Allow-Headers", "x-requested-with").build();
        }
    }

    @POST
    @Path("movie/user")
    @Produces("application/json")
    public Response MovieUser(@FormParam("rut") @DefaultValue("") String rut, @FormParam("name") @DefaultValue("") String name,
            @FormParam("address") @DefaultValue("") String address, @FormParam("phone") @DefaultValue("") String phone,
            @FormParam("typeuser") int typeuser, @FormParam("user") @DefaultValue("") String user,
            @FormParam("pass") @DefaultValue("") String pass) {

        if (rut != null && name != null && address != null && typeuser > 0 && user != null && pass != null) {
            BeanUser bu = new BeanUser(rut, name, address, phone, typeuser, user, pass);

            return Response.ok(controll.User(bu))
                    .header("Access-Control-Allow-Origin", "*")
                    .header("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE")
                    .header("Access-Control-Max-Age", "3600")
                    .header("Access-Control-Allow-Headers", "x-requested-with").build();
        } else {
            return Response.status(Response.Status.UNAUTHORIZED).header("Access-Control-Allow-Origin", "*")
                    .header("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE")
                    .header("Access-Control-Max-Age", "3600")
                    .header("Access-Control-Allow-Headers", "x-requested-with").build();
        }
    }

    @POST
    @Path("movie/createorder")
    @Produces("application/json")
    public Response MovieCreateOrder(@FormParam("idmovie") int idMovie, @FormParam("numreserv") int numreserv,
            @FormParam("idstate") int idstate, @Context HttpHeaders hh) {
        MultivaluedMap<String, String> headerParams = hh.getRequestHeaders();
        if (headerParams.get("iduser") != null && headerParams.get("token") != null && idMovie > 0) {
            BeanOrder bo = new BeanOrder(headerParams.get("token").get(0), Integer.parseInt(headerParams.get("iduser").get(0)),
                    idMovie, numreserv, idstate);

            return Response.ok(controll.Order(bo))
                    .header("Access-Control-Allow-Origin", "*")
                    .header("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE")
                    .header("Access-Control-Max-Age", "3600")
                    .header("Access-Control-Allow-Headers", "x-requested-with").build();
        } else {
            return Response.status(Response.Status.UNAUTHORIZED).header("Access-Control-Allow-Origin", "*")
                    .header("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE")
                    .header("Access-Control-Max-Age", "3600")
                    .header("Access-Control-Allow-Headers", "x-requested-with").build();
        }
    }

    @POST
    @Path("movie")
    @Produces("application/json")
    public Response Movie(@FormParam("idmovie") int idmovie, @FormParam("idstate") int idstate,
            @FormParam("idactor") int idactor, @FormParam("iddirector") int iddirector) {
        BeanSelectMovies bsm = new BeanSelectMovies("", 0, idmovie, idstate, idactor, iddirector);
        return Response.ok(controll.Movie(bsm))
                .header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE")
                .header("Access-Control-Max-Age", "3600")
                .header("Access-Control-Allow-Headers", "x-requested-with").build();

    }

    @POST
    @Path("movie/order")
    @Produces("application/json")
    public Response MovieOrder(@FormParam("idorder") String order, @FormParam("idstate") int idstate,
            @FormParam("dates") String datestart, @FormParam("datef") String datefinish,
            @Context HttpHeaders hh) {
        MultivaluedMap<String, String> headerParams = hh.getRequestHeaders();
        if (headerParams.get("iduser") != null && headerParams.get("token") != null) {
            BeanSearchOrder bso = new BeanSearchOrder(headerParams.get("token").get(0),
                    Integer.parseInt(headerParams.get("iduser").get(0)), order, idstate, datestart, datefinish);

            return Response.ok(controll.MovieOrder(bso))
                    .header("Access-Control-Allow-Origin", "*")
                    .header("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE")
                    .header("Access-Control-Max-Age", "3600")
                    .header("Access-Control-Allow-Headers", "x-requested-with").build();
        } else {
            return Response.status(Response.Status.UNAUTHORIZED).header("Access-Control-Allow-Origin", "*")
                    .header("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE")
                    .header("Access-Control-Max-Age", "3600")
                    .header("Access-Control-Allow-Headers", "x-requested-with").build();
        }
    }

    @POST
    @Path("movie/detail")
    @Produces("application/json")
    public Response MovieDetail(@FormParam("idorder") int idorder, @Context HttpHeaders hh) {
        MultivaluedMap<String, String> headerParams = hh.getRequestHeaders();

        if (headerParams.get("iduser") != null && headerParams.get("token") != null && idorder > 0) {
            BeanOrderDetail bod = new BeanOrderDetail(headerParams.get("token").get(0), Integer.parseInt(headerParams.get("iduser").get(0)),
                    idorder);

            return Response.ok(controll.Detail(bod))
                    .header("Access-Control-Allow-Origin", "*")
                    .header("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE")
                    .header("Access-Control-Max-Age", "3600")
                    .header("Access-Control-Allow-Headers", "x-requested-with").build();
        } else {
            return Response.status(Response.Status.UNAUTHORIZED).header("Access-Control-Allow-Origin", "*")
                    .header("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE")
                    .header("Access-Control-Max-Age", "3600")
                    .header("Access-Control-Allow-Headers", "x-requested-with").build();
        }
    }

    @POST
    @Path("movie/actor")
    @Produces("application/json")
    public Response MovieListActor(@FormParam("idmovie") int idmovie, @FormParam("idactor") int idactor) {
        BeanActor blr = new BeanActor("", 0, idmovie, idactor);

        return Response.ok(controll.ListActor(blr))
                .header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE")
                .header("Access-Control-Max-Age", "3600")
                .header("Access-Control-Allow-Headers", "x-requested-with").build();
    }

    @POST
    @Path("movie/director")
    @Produces("application/json")
    public Response MovieListDirector(@FormParam("iddirector") int iddirector) {
        BeanDirector blr = new BeanDirector("", 0, iddirector);

        return Response.ok(controll.ListDirector(blr))
                .header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE")
                .header("Access-Control-Max-Age", "3600")
                .header("Access-Control-Allow-Headers", "x-requested-with").build();
    }

    @POST
    @Path("movie/state")
    @Produces("application/json")
    public Response MovieListState(@FormParam("idstate") int idstate, @Context HttpHeaders hh) {
        MultivaluedMap<String, String> headerParams = hh.getRequestHeaders();
        if (headerParams.get("iduser") != null && headerParams.get("token") != null) {
            BeanState blr = new BeanState(headerParams.get("token").get(0), Integer.parseInt(headerParams.get("iduser").get(0)), idstate);

            return Response.ok(controll.ListState(blr))
                    .header("Access-Control-Allow-Origin", "*")
                    .header("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE")
                    .header("Access-Control-Max-Age", "3600")
                    .header("Access-Control-Allow-Headers", "x-requested-with").build();
        } else {
            return Response.status(Response.Status.UNAUTHORIZED).header("Access-Control-Allow-Origin", "*")
                    .header("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE")
                    .header("Access-Control-Max-Age", "3600")
                    .header("Access-Control-Allow-Headers", "x-requested-with").build();
        }
    }

    @POST
    @Path("movie/listmovie")
    @Produces("application/json")
    public Response MovieListMovie(@FormParam("idmovie") int idmovie) {
        BeanMovies blr = new BeanMovies("", 0, idmovie);

        return Response.ok(controll.ListMovies(blr))
                .header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE")
                .header("Access-Control-Max-Age", "3600")
                .header("Access-Control-Allow-Headers", "x-requested-with").build();
    }

    @POST
    @Path("movie/logout")
    @Produces("application/json")
    public Response MovieLogout(@Context HttpHeaders hh) {
        MultivaluedMap<String, String> headerParams = hh.getRequestHeaders();

        if (headerParams.get("iduser") != null && headerParams.get("token") != null) {
            BeanSession bs = new BeanSession(Integer.parseInt(headerParams.get("iduser").get(0)), headerParams.get("token").get(0));

            return Response.ok(controll.OutSession(bs))
                    .header("Access-Control-Allow-Origin", "*")
                    .header("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE")
                    .header("Access-Control-Max-Age", "3600")
                    .header("Access-Control-Allow-Headers", "x-requested-with").build();
        } else {
            return Response.status(Response.Status.UNAUTHORIZED).header("Access-Control-Allow-Origin", "*")
                    .header("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE")
                    .header("Access-Control-Max-Age", "3600")
                    .header("Access-Control-Allow-Headers", "x-requested-with").build();
        }
    }
}

package com.manage;

import com.model.BeanActor;
import com.model.BeanDirector;
import com.model.BeanMovies;
import com.model.BeanOrder;
import com.model.BeanOrderDetail;
import com.model.BeanSearchOrder;
import com.model.BeanUser;
import com.model.BeanSelectMovies;
import com.model.BeanSession;
import com.model.BeanState;

/**
 *
 * @author 81001587
 */
public class Exceptions {

    public Exceptions() {
    }

    /**
     * ProcessError
     *
     * @param endPointError 1=login 2=createuser 3=createorder 4=consulmovie
     * 5=searchorder 6=orderdetail 7=actor 8=director 9=status 10=movies 11=logout
     * @return
     */
    protected Object ProcessError(int endPointError) {
        String msj = null;
        Object obj = new Object();
        switch (endPointError) {
            case 1:
                msj = "No se ha podido iniciar sesión, por favor revise.";
                obj = new BeanSession(0, "","", msj, true);
                break;
            case 2:
                msj = "No fue posible crear el usuario, por favor intente mas tarde.";                
                obj = new BeanUser( msj, true);
                break;
            case 3:
                msj = "No fue posible crear la orden, por favor intente mas tarde."; 
                obj = new BeanOrder( msj, true);
                break;
            case 4:
                msj = "No se encontraron registros de peliculas.";
                obj = new BeanSelectMovies(msj, true);
                break;
            case 5:
                msj = "No se encontraron ordenes generada.";
                obj = new BeanSearchOrder(msj, true);
                break;
            case 6:
                msj = "No se encontro el detalle de dicha orden.";
                obj = new BeanOrderDetail(msj, true);
                break;
            case 7:
                msj = "No fue posible listar los actores, intente mas tarde.";
                obj = new BeanActor(msj, true);
                break;
            case 8:
                msj = "No fue posible listar los directores, intente mas tarde.";
                obj = new BeanDirector(msj, true);
                break;
            case 9:
                msj = "No fue posible listar los estados, intente mas tarde.";
                obj = new BeanState(msj, true);
                break;
            case 10:
                msj = "No fue posible listar las peliculas, intente mas tarde.";
                obj = new BeanMovies(msj, true);
                break;
            case 11:
                msj = "No se ha podido cerrar sesión.";
                obj = new BeanSession(msj, true);
                break;

        }
        return obj;
    }

}

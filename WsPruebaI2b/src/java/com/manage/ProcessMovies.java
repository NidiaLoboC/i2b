package com.manage;

import com.database.CallProcedures;
import com.database.Conex;
import com.google.gson.Gson;
import com.model.BeanActor;
import com.model.BeanDirector;
import com.model.BeanMovies;
import com.model.BeanOrder;
import com.model.BeanOrderDetail;
import com.model.BeanUser;
import com.model.BeanSearchOrder;
import com.model.BeanSelectMovies;
import com.model.BeanSession;
import com.model.BeanState;
import java.sql.SQLException;

/**
 *
 * @author 81001587
 */
public class ProcessMovies {

    private CallProcedures callPro = new CallProcedures();
    private FunctionsMovies funtions = new FunctionsMovies();
    private Gson gson = new Gson();
    private boolean error = true;
    private Exceptions exc = new Exceptions();
    private CallProcedures cp = new CallProcedures();
    private String nameMethod, nameClass;

    public String GetTokenSession(BeanSession bs) {
        nameMethod = new Object() {
        }.getClass().getEnclosingMethod().getName();
        nameClass = this.getClass().getSimpleName();
        Conex con = new Conex();
        try {
            return gson.toJson(callPro.procCreateToken(con.conection(), bs));
        } catch (SQLException ex) {
            String message = (ex.getMessage().length() > 1000) ? ex.getMessage().substring(0, 1000) : ex.getMessage();
            cp.procLoggerRecord(ex.getErrorCode(), message, nameMethod, nameClass);
            return gson.toJson(exc.ProcessError(1));
        }
    }

    public String UserCreation(BeanUser bu) {
        nameMethod = new Object() {
        }.getClass().getEnclosingMethod().getName();
        nameClass = this.getClass().getSimpleName();
        Conex con = new Conex();
        try {

            return gson.toJson(callPro.createUser(con.conection(), bu));
        } catch (SQLException ex) {
            String message = (ex.getMessage().length() > 1000) ? ex.getMessage().substring(0, 1000) : ex.getMessage();
            cp.procLoggerRecord(ex.getErrorCode(), message, nameMethod, nameClass);
            return gson.toJson(exc.ProcessError(2));
        }
    }

    public String OrderCreation(BeanOrder bo) {
        nameMethod = new Object() {
        }.getClass().getEnclosingMethod().getName();
        nameClass = this.getClass().getSimpleName();
        Conex con = new Conex();
        try {

            return gson.toJson(callPro.createReservation(con.conection(), bo));

        } catch (SQLException ex) {
            String message = (ex.getMessage().length() > 1000) ? ex.getMessage().substring(0, 1000) : ex.getMessage();
            cp.procLoggerRecord(ex.getErrorCode(), message, nameMethod, nameClass);
            return gson.toJson(exc.ProcessError(3));
        }
    }

    public String CheckMovie(BeanSelectMovies bsm) {
        Conex con = new Conex();

        return gson.toJson(funtions.fConsultMovies(con.conection(), bsm));
    }

    public String CheckMovieOrder(BeanSearchOrder bso) {
        Conex con = new Conex();

        return gson.toJson(funtions.fConsultMoviesOrder(con.conection(), bso));
    }

    public String OrderDetail(BeanOrderDetail bod) {
        Conex con = new Conex();

        return gson.toJson(funtions.fOrderDetail(con.conection(), bod));
    }

    public String Actor(BeanActor ba) {
        Conex con = new Conex();

        return gson.toJson(funtions.fListActor(con.conection(), ba));
    }

    public String Director(BeanDirector bd) {
        Conex con = new Conex();

        return gson.toJson(funtions.fListDirector(con.conection(), bd));
    }

    public String State(BeanState bs) {
        Conex con = new Conex();

        return gson.toJson(funtions.fListState(con.conection(), bs));
    }

    public String Movies(BeanMovies bm) {
        Conex con = new Conex();

        return gson.toJson(funtions.fListMovie(con.conection(), bm));
    }

    public String OutTokendSession(BeanSession bs) {
        nameMethod = new Object() {
        }.getClass().getEnclosingMethod().getName();
        nameClass = this.getClass().getSimpleName();
        Conex con = new Conex();
        try {
            return gson.toJson(callPro.procInvalidateToken(con.conection(), bs));
        } catch (SQLException ex) {
            String message = (ex.getMessage().length() > 1000) ? ex.getMessage().substring(0, 1000) : ex.getMessage();
            cp.procLoggerRecord(ex.getErrorCode(), message, nameMethod, nameClass);

            return gson.toJson(exc.ProcessError(11));
        }
    }

}

package com.manage;

import com.database.CallProcedures;
import com.google.gson.Gson;
import com.model.BeanActor;
import com.model.BeanDirector;
import com.model.BeanMovies;
import com.model.BeanOrderDetail;
import com.model.BeanSearchOrder;
import com.model.BeanSelectMovies;
import com.model.BeanState;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author 81001587
 */
public class FunctionsMovies {

    private CallProcedures cp = new CallProcedures();
    private Exceptions exc = new Exceptions();
    private Gson gson = new Gson();

    public FunctionsMovies() {
    }

    public Object fConsultMovies(Connection con, BeanSelectMovies bsm) {
        List<BeanSelectMovies> listbsm = new ArrayList<>();
        String nameMethod = new Object() {
        }.getClass().getEnclosingMethod().getName(),
                nameClass = this.getClass().getSimpleName();
        String sql = "SELECT * FROM public.fr_consulta_pelicula(?,?,?,?,?,?)";
        try {
            PreparedStatement pr = con.prepareStatement(sql);
            pr.setString(1, bsm.getToken());
            pr.setInt(2, bsm.getIdUser());
            pr.setInt(3, bsm.getIdMovies());
            pr.setInt(4, bsm.getStatus());
            pr.setInt(5, bsm.getIdActor());
            pr.setInt(6, bsm.getIdDirector());
            ResultSet rs = (ResultSet) pr.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    listbsm.add(new BeanSelectMovies(rs.getInt("ID_PELICULA"), rs.getString("NOMBRE"),rs.getString("DESCRIPCION"), rs.getInt("NUM_PELICULAS"),
                            rs.getInt("NUMERO_DISPONIBLES"), rs.getInt("PRESTADAS"), rs.getBoolean("DISPONIBILIDAD")));
                }
                rs.close();
                pr.close();
                return listbsm;
            } else {
                return exc.ProcessError(4);
            }
        } catch (SQLException ex) {
            String message = (ex.getMessage().length() > 1000) ? ex.getMessage().substring(0, 1000) : ex.getMessage();
            cp.procLoggerRecord(ex.getErrorCode(), message, nameMethod, nameClass);
            return exc.ProcessError(4);
        } finally {
            try {
                con.close();
            } catch (SQLException ex) {
                String message = (ex.getMessage().length() > 1000) ? ex.getMessage().substring(0, 1000) : ex.getMessage();
                cp.procLoggerRecord(ex.getErrorCode(), message, nameMethod, nameClass);
            }
        }
    }

    public Object fConsultMoviesOrder(Connection con, BeanSearchOrder bso) {
        List<BeanSearchOrder> listbso = new ArrayList<>();
        String nameMethod = new Object() {
        }.getClass().getEnclosingMethod().getName(),
                nameClass = this.getClass().getSimpleName();
        String sql = "SELECT * FROM public.fr_consultar_ordenes(?,?,?,?,?,?)";
        try {
            PreparedStatement pr = con.prepareStatement(sql);
            pr.setString(1, bso.getToken());
            pr.setInt(2, bso.getIdUser());
            pr.setString(3, ((bso.getOrder().length()>1)?bso.getOrder():null));
            pr.setInt(4, bso.getIdStatus());
            pr.setString(5, ((bso.getDateStart().length()>1)?bso.getDateStart():null));
            pr.setString(6,((bso.getDateFinish().length()>1)?bso.getDateFinish():null));
            ResultSet rs = (ResultSet) pr.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    listbso.add(new BeanSearchOrder(rs.getInt("ID_ORDEN"),rs.getString("ORDEN"), rs.getString("PELICULA"), rs.getString("USUARIO"),
                            rs.getString("ESTADO")));
                }
                rs.close();
                pr.close();
                return listbso;
            } else {
                return exc.ProcessError(5);
            }
        } catch (SQLException ex) {
            String message = (ex.getMessage().length() > 1000) ? ex.getMessage().substring(0, 1000) : ex.getMessage();
            cp.procLoggerRecord(ex.getErrorCode(), message, nameMethod, nameClass);
            return exc.ProcessError(5);
        } finally {
            try {
                con.close();
            } catch (SQLException ex) {
                String message = (ex.getMessage().length() > 1000) ? ex.getMessage().substring(0, 1000) : ex.getMessage();
                cp.procLoggerRecord(ex.getErrorCode(), message, nameMethod, nameClass);
            }
        }
    }

    public Object fOrderDetail(Connection con, BeanOrderDetail bod) {
        List<BeanOrderDetail> listbod = new ArrayList<>();
        String nameMethod = new Object() {
        }.getClass().getEnclosingMethod().getName(),
                nameClass = this.getClass().getSimpleName();
        String sql = "SELECT * FROM public.fr_detalle_orden(?,?,?)";
        try {
            PreparedStatement pr = con.prepareStatement(sql);
            pr.setString(1, bod.getToken());
            pr.setInt(2, bod.getIdUser());
            pr.setInt(3, bod.getIdOrder());
            ResultSet rs = (ResultSet) pr.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    listbod.add(new BeanOrderDetail(rs.getString("CODIGO_ORDEN"), rs.getString("ESTADO_ANTERIOR"), rs.getString("ESTADO_ACTUAL"),
                            rs.getString("FECHA_MODIFICACION"), rs.getInt("NUM_PRESTADAS")));
                }
                rs.close();
                pr.close();
                return listbod;
            } else {
                return exc.ProcessError(6);
            }
        } catch (SQLException ex) {
            String message = (ex.getMessage().length() > 1000) ? ex.getMessage().substring(0, 1000) : ex.getMessage();
            cp.procLoggerRecord(ex.getErrorCode(), message, nameMethod, nameClass);
            return exc.ProcessError(6);
        } finally {
            try {
                con.close();
            } catch (SQLException ex) {
                String message = (ex.getMessage().length() > 1000) ? ex.getMessage().substring(0, 1000) : ex.getMessage();
                cp.procLoggerRecord(ex.getErrorCode(), message, nameMethod, nameClass);
            }
        }
    }

    public Object fListActor(Connection con, BeanActor ba) {
        List<BeanActor> listba = new ArrayList<>();
        String nameMethod = new Object() {
        }.getClass().getEnclosingMethod().getName(),
                nameClass = this.getClass().getSimpleName();
        String sql = "SELECT * FROM public.fr_listar_actor(?,?,?,?)";
        try {
            PreparedStatement pr = con.prepareStatement(sql);
            pr.setString(1, ba.getToken());
            pr.setInt(2, ba.getIdUser());
            pr.setInt(3, ba.getIdMovie());
            pr.setInt(4, ba.getIdActor());
            ResultSet rs = (ResultSet) pr.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    listba.add(new BeanActor(rs.getInt("ID_ACTOR"), rs.getString("NOMBRE")));
                }
                rs.close();
                pr.close();
                return listba;
            } else {
                return exc.ProcessError(7);
            }
        } catch (SQLException ex) {
            String message = (ex.getMessage().length() > 1000) ? ex.getMessage().substring(0, 1000) : ex.getMessage();
            cp.procLoggerRecord(ex.getErrorCode(), message, nameMethod, nameClass);
            return exc.ProcessError(7);
        } finally {
            try {
                con.close();
            } catch (SQLException ex) {
                String message = (ex.getMessage().length() > 1000) ? ex.getMessage().substring(0, 1000) : ex.getMessage();
                cp.procLoggerRecord(ex.getErrorCode(), message, nameMethod, nameClass);
            }
        }
    }

    public Object fListDirector(Connection con, BeanDirector bd) {
        List<BeanDirector> listbd = new ArrayList<>();
        String nameMethod = new Object() {
        }.getClass().getEnclosingMethod().getName(),
                nameClass = this.getClass().getSimpleName();
        String sql = "SELECT * FROM public.fr_listar_director(?,?,?)";
        try {
            PreparedStatement pr = con.prepareStatement(sql);
            pr.setString(1, bd.getToken());
            pr.setInt(2, bd.getIdUser());
            pr.setInt(3, bd.getIdDirector());
            ResultSet rs = (ResultSet) pr.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    listbd.add(new BeanDirector(rs.getInt("ID_DIRECTOR"), rs.getString("NOMBRE")));
                }
                rs.close();
                pr.close();
                return listbd;
            } else {
                return exc.ProcessError(8);
            }
        } catch (SQLException ex) {
            String message = (ex.getMessage().length() > 1000) ? ex.getMessage().substring(0, 1000) : ex.getMessage();
            cp.procLoggerRecord(ex.getErrorCode(), message, nameMethod, nameClass);
            return exc.ProcessError(8);
        } finally {
            try {
                con.close();
            } catch (SQLException ex) {
                String message = (ex.getMessage().length() > 1000) ? ex.getMessage().substring(0, 1000) : ex.getMessage();
                cp.procLoggerRecord(ex.getErrorCode(), message, nameMethod, nameClass);
            }
        }
    }

    public Object fListState(Connection con, BeanState bs) {
        List<BeanState> listbs = new ArrayList<>();
        String nameMethod = new Object() {
        }.getClass().getEnclosingMethod().getName(),
                nameClass = this.getClass().getSimpleName();
        String sql = "SELECT * FROM public.fr_listar_estado(?,?,?)";
        try {
            PreparedStatement pr = con.prepareStatement(sql);
            pr.setString(1, bs.getToken());
            pr.setInt(2, bs.getIdUser());
            pr.setInt(3, bs.getIdState());
            ResultSet rs = (ResultSet) pr.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    listbs.add(new BeanState(rs.getInt("ID_ESTADO"), rs.getString("DESCRIPCION")));
                }
                rs.close();
                pr.close();
                return listbs;
            } else {
                return exc.ProcessError(9);
            }
        } catch (SQLException ex) {
            String message = (ex.getMessage().length() > 1000) ? ex.getMessage().substring(0, 1000) : ex.getMessage();
            cp.procLoggerRecord(ex.getErrorCode(), message, nameMethod, nameClass);
            return exc.ProcessError(9);
        } finally {
            try {
                con.close();
            } catch (SQLException ex) {
                String message = (ex.getMessage().length() > 1000) ? ex.getMessage().substring(0, 1000) : ex.getMessage();
                cp.procLoggerRecord(ex.getErrorCode(), message, nameMethod, nameClass);
            }
        }
    }

    public Object fListMovie(Connection con, BeanMovies bm) {
        List<BeanMovies> listbm = new ArrayList<>();
        String nameMethod = new Object() {
        }.getClass().getEnclosingMethod().getName(),
                nameClass = this.getClass().getSimpleName();
        String sql = "SELECT * FROM public.fr_listar_pelicula(?,?,?)";
        try {
            PreparedStatement pr = con.prepareStatement(sql);
            pr.setString(1, bm.getToken());
            pr.setInt(2, bm.getIdUser());
            pr.setInt(3, bm.getIdMovie());
            ResultSet rs = (ResultSet) pr.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    listbm.add(new BeanMovies(rs.getInt("ID_PELICULA"), rs.getString("NOMBRE")));
                }
                rs.close();
                pr.close();
                return listbm;
            } else {
                return exc.ProcessError(10);
            }
        } catch (SQLException ex) {
            String message = (ex.getMessage().length() > 1000) ? ex.getMessage().substring(0, 1000) : ex.getMessage();
            cp.procLoggerRecord(ex.getErrorCode(), message, nameMethod, nameClass);
            return exc.ProcessError(10);
        } finally {
            try {
                con.close();
            } catch (SQLException ex) {
                String message = (ex.getMessage().length() > 1000) ? ex.getMessage().substring(0, 1000) : ex.getMessage();
                cp.procLoggerRecord(ex.getErrorCode(), message, nameMethod, nameClass);
            }
        }
    }

}

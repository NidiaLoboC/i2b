package com.manage;

import com.model.BeanActor;
import com.model.BeanDirector;
import com.model.BeanMovies;
import com.model.BeanOrder;
import com.model.BeanOrderDetail;
import com.model.BeanUser;
import com.model.BeanSearchOrder;
import com.model.BeanSelectMovies;
import com.model.BeanSession;
import com.model.BeanState;

/**
 *
 * @author 81001587
 */
public class Controller {

    ProcessMovies procR = new ProcessMovies();

    public String GetSession(BeanSession bs) {
        return procR.GetTokenSession(bs);
    }

    public String User(BeanUser bu) {
        return procR.UserCreation(bu);
    }

    public String Order(BeanOrder bo) {
        return procR.OrderCreation(bo);
    }

    public String Movie(BeanSelectMovies bsm) {
        return procR.CheckMovie(bsm);
    }

    public String MovieOrder(BeanSearchOrder bso) {
        return procR.CheckMovieOrder(bso);
    }

    public String Detail(BeanOrderDetail bod) {
        return procR.OrderDetail(bod);
    }

    public String ListActor(BeanActor ba) {
        return procR.Actor(ba);
    }

    public String ListDirector(BeanDirector bd) {
        return procR.Director(bd);
    }

    public String ListState(BeanState bs) {
        return procR.State(bs);
    }

    public String ListMovies(BeanMovies bm) {
        return procR.Movies(bm);
    }

    public String OutSession(BeanSession bs) {
        return procR.GetTokenSession(bs);
    }

}

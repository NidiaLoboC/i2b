/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.model;

/**
 *
 * @author 81001587
 */
public class BeanOrder {

    private int idOrder;
    private String codOrder;
    private String msgError;
    private boolean error;
    private String p_token;
    private int p_id_usuario;
    private int p_id_pelicula;
    private int p_numero_reserva;
    private int p_id_estado;

    public BeanOrder() {
    }

    public BeanOrder(int idOrder,String codOrder, String msgError, boolean error) {
        this.codOrder = codOrder;
        this.idOrder = idOrder;
        this.msgError = msgError;
        this.error = error;
    }

    public BeanOrder(String msgError, boolean error) {
        this.msgError = msgError;
        this.error = error;
    }

    public BeanOrder(String p_token, int p_id_usuario, int p_id_pelicula, int p_numero_reserva, int p_id_estado) {
        this.p_token = p_token;
        this.p_id_usuario = p_id_usuario;
        this.p_id_pelicula = p_id_pelicula;
        this.p_numero_reserva = p_numero_reserva;
        this.p_id_estado = p_id_estado;
    }

    public int getIdOrder() {
        return idOrder;
    }

    public void setIdOrder(int idOrder) {
        this.idOrder = idOrder;
    }

    public String getMsgError() {
        return msgError;
    }

    public void setMsgError(String msgError) {
        this.msgError = msgError;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public String getP_token() {
        return p_token;
    }

    public void setP_token(String p_token) {
        this.p_token = p_token;
    }

    public int getP_id_usuario() {
        return p_id_usuario;
    }

    public void setP_id_usuario(int p_id_usuario) {
        this.p_id_usuario = p_id_usuario;
    }

    public int getP_id_pelicula() {
        return p_id_pelicula;
    }

    public void setP_id_pelicula(int p_id_pelicula) {
        this.p_id_pelicula = p_id_pelicula;
    }

    public int getP_numero_reserva() {
        return p_numero_reserva;
    }

    public void setP_numero_reserva(int p_numero_reserva) {
        this.p_numero_reserva = p_numero_reserva;
    }

    public int getP_id_estado() {
        return p_id_estado;
    }

    public void setP_id_estado(int p_id_estado) {
        this.p_id_estado = p_id_estado;
    }

    public String getCodOrder() {
        return codOrder;
    }

    public void setCodOrder(String codOrder) {
        this.codOrder = codOrder;
    }

}

package com.model;

/**
 *
 * @author 81001587
 */
public class BeanSearchOrder {

    private int idOrder;
    private String order;
    private String movie;
    private String user;
    private String status;
    private int idStatus;
    private String msgError;
    private boolean error;
    private int idUser;
    private String token;
    private String dateStart;
    private String dateFinish;

    public BeanSearchOrder() {
    }

    public BeanSearchOrder(String msgError, boolean error) {
        this.msgError = msgError;
        this.error = error;
    }

    public BeanSearchOrder(int idUser, String token) {
        this.idUser = idUser;
        this.token = token;
    }

    public BeanSearchOrder(int idOrder,String order, String movie, String user, String status) {
        this.idOrder = idOrder;
        this.order = order;
        this.movie = movie;
        this.user = user;
        this.status = status;
    }

    public BeanSearchOrder(String token, int idUser, String order, int idStatus, String dateStart, String dateFinish) {
        this.order = order;
        this.idStatus = idStatus;
        this.idUser = idUser;
        this.token = token;
        this.dateStart = dateStart;
        this.dateFinish = dateFinish;
    }

    public int getIdOrder() {
        return idOrder;
    }

    public void setIdOrder(int idOrder) {
        this.idOrder = idOrder;
    }

    public String getOrder() {
        return order;
    }

    public void setOrder(String order) {
        this.order = order;
    }

    public String getMovie() {
        return movie;
    }

    public void setMovie(String movie) {
        this.movie = movie;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getIdStatus() {
        return idStatus;
    }

    public void setIdStatus(int idStatus) {
        this.idStatus = idStatus;
    }

    public String getMsgError() {
        return msgError;
    }

    public void setMsgError(String msgError) {
        this.msgError = msgError;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public int getIdUser() {
        return idUser;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getDateStart() {
        return dateStart;
    }

    public void setDateStart(String dateStart) {
        this.dateStart = dateStart;
    }

    public String getDateFinish() {
        return dateFinish;
    }

    public void setDateFinish(String dateFinish) {
        this.dateFinish = dateFinish;
    }

}

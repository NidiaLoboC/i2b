/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.model;

/**
 *
 * @author 81001587
 */
public class BeanMovies {

    private String token;
    private int idUser;
    private int idMovie;
    private String nameMovie;
    private String msgError;
    private boolean error;

    public BeanMovies() {
    }

    public BeanMovies(String token, int idUser, int idMovie) {
        this.token = token;
        this.idUser = idUser;
        this.idMovie = idMovie;
    }

    public BeanMovies(int idMovie, String nameMovie) {
        this.idMovie = idMovie;
        this.nameMovie = nameMovie;
    }

    public BeanMovies(String msgError, boolean error) {
        this.msgError = msgError;
        this.error = error;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public int getIdUser() {
        return idUser;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    public int getIdMovie() {
        return idMovie;
    }

    public void setIdMovie(int idMovie) {
        this.idMovie = idMovie;
    }

    public String getNameMovie() {
        return nameMovie;
    }

    public void setNameMovie(String nameMovie) {
        this.nameMovie = nameMovie;
    }

    public String getMsgError() {
        return msgError;
    }

    public void setMsgError(String msgError) {
        this.msgError = msgError;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

}

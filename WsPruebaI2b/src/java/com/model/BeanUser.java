package com.model;

/**
 *
 * @author 81001587
 */
public class BeanUser {

    private int iduser;
    private String msgError;
    private boolean error;
    private String p_rut;
    private String p_nombre;
    private String p_direccion;
    private String p_telefono;
    private int p_id_tipo_usuario;
    private String p_user;
    private String p_pass;

    public BeanUser() {
    }

    public BeanUser(String msgError, boolean error) {
        this.msgError = msgError;
        this.error = error;
    }

    public BeanUser(int iduser, String msgError, boolean error) {
        this.iduser = iduser;
        this.msgError = msgError;
        this.error = error;
    }

    public BeanUser(String p_rut, String p_nombre, String p_direccion, String p_telefono, int p_id_tipo_usuario, String p_user, String p_pass) {
        this.p_rut = p_rut;
        this.p_nombre = p_nombre;
        this.p_direccion = p_direccion;
        this.p_telefono = p_telefono;
        this.p_id_tipo_usuario = p_id_tipo_usuario;
        this.p_user = p_user;
        this.p_pass = p_pass;
    }

    public int getIduser() {
        return iduser;
    }

    public void setIduser(int iduser) {
        this.iduser = iduser;
    }

    public String getMsgError() {
        return msgError;
    }

    public void setMsgError(String msgError) {
        this.msgError = msgError;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public String getP_rut() {
        return p_rut;
    }

    public void setP_rut(String p_rut) {
        this.p_rut = p_rut;
    }

    public String getP_nombre() {
        return p_nombre;
    }

    public void setP_nombre(String p_nombre) {
        this.p_nombre = p_nombre;
    }

    public String getP_direccion() {
        return p_direccion;
    }

    public void setP_direccion(String p_direccion) {
        this.p_direccion = p_direccion;
    }

    public String getP_telefono() {
        return p_telefono;
    }

    public void setP_telefono(String p_telefono) {
        this.p_telefono = p_telefono;
    }

    public int getP_id_tipo_usuario() {
        return p_id_tipo_usuario;
    }

    public void setP_id_tipo_usuario(int p_id_tipo_usuario) {
        this.p_id_tipo_usuario = p_id_tipo_usuario;
    }

    public String getP_user() {
        return p_user;
    }

    public void setP_user(String p_user) {
        this.p_user = p_user;
    }

    public String getP_pass() {
        return p_pass;
    }

    public void setP_pass(String p_pass) {
        this.p_pass = p_pass;
    }

}

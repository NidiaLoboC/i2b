/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.model;

/**
 *
 * @author 81001587
 */
public class BeanOrderDetail {

    private String token;
    private int idUser;
    private int idOrder;
    private String order;
    private String previousState;
    private String actualState;
    private String modificationDate;
    private int numBorrowedMovie;
    private String msgError;
    private boolean error;

    public BeanOrderDetail() {
    }

    public BeanOrderDetail(String token, int idUser, int idOrder) {
        this.token = token;
        this.idUser = idUser;
        this.idOrder = idOrder;
    }

    public BeanOrderDetail(String order, String previousState, String actualState, String modificationDate, int numBorrowedMovie) {
        this.order = order;
        this.previousState = previousState;
        this.actualState = actualState;
        this.modificationDate = modificationDate;
        this.numBorrowedMovie = numBorrowedMovie;
    }

    public BeanOrderDetail(String msgError, boolean error) {
        this.msgError = msgError;
        this.error = error;
    }

    public int getNumBorrowedMovie() {
        return numBorrowedMovie;
    }

    public void setNumBorrowedMovie(int numBorrowedMovie) {
        this.numBorrowedMovie = numBorrowedMovie;
    }

    public String getOrder() {
        return order;
    }

    public void setOrder(String order) {
        this.order = order;
    }

    public String getPreviousState() {
        return previousState;
    }

    public void setPreviousState(String previousState) {
        this.previousState = previousState;
    }

    public String getActualState() {
        return actualState;
    }

    public void setActualState(String actualState) {
        this.actualState = actualState;
    }

    public String getModificationDate() {
        return modificationDate;
    }

    public void setModificationDate(String modificationDate) {
        this.modificationDate = modificationDate;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public int getIdUser() {
        return idUser;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    public int getIdOrder() {
        return idOrder;
    }

    public void setIdOrder(int idOrder) {
        this.idOrder = idOrder;
    }

    public String getMsgError() {
        return msgError;
    }

    public void setMsgError(String msgError) {
        this.msgError = msgError;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

}

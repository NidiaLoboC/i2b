package com.model;

/**
 *
 * @author 81001587
 */
public class BeanSession {

    private int idUser;
    private String token;
    private String msgError;
    private String name;
    private String user;
    private String pass;
    private boolean error;

    public BeanSession() {
    }

    public BeanSession(int idUser, String token, String name, String msgError, boolean error) {
        this.idUser = idUser;
        this.name = name;
        this.token = token;
        this.msgError = msgError;
        this.error = error;
    }

    public BeanSession(String user, String pass) {
        this.user = user;
        this.pass = pass;
    }

    public BeanSession(int idUser, String token) {
        this.idUser = idUser;
        this.token = token;
    }

    public BeanSession(String msgError, boolean error) {
        this.msgError = msgError;
        this.error = error;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getMsgError() {
        return msgError;
    }

    public void setMsgError(String msgError) {
        this.msgError = msgError;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public int getIdUser() {
        return idUser;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}

package com.database;

import com.model.BeanUser;
import com.model.BeanOrder;
import com.model.BeanSession;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author 81001587
 */
public class CallProcedures {

    private boolean error;
    private String msgError;

    public List<BeanSession> procCreateToken(Connection con, BeanSession bs) throws SQLException {
        List<BeanSession> bsession = new ArrayList<>();
        String token,name;
        int iduser;
        con.setAutoCommit(false);
        try (CallableStatement cs = con.prepareCall("CALL public.pr_login(?,?,?,?,?,?,?)")) {
            cs.setString(1, bs.getUser());
            cs.setString(2, bs.getPass());
            cs.setNull(3, java.sql.Types.INTEGER);
            cs.setNull(4, java.sql.Types.VARCHAR);
            cs.setNull(5, java.sql.Types.VARCHAR);
            cs.setNull(6, java.sql.Types.VARCHAR);
            cs.setNull(7, java.sql.Types.BOOLEAN);
            cs.registerOutParameter(3, Types.INTEGER);
            cs.registerOutParameter(4, Types.VARCHAR);
            cs.registerOutParameter(5, Types.VARCHAR);
            cs.registerOutParameter(6, Types.VARCHAR);
            cs.registerOutParameter(7, Types.BOOLEAN);
            cs.executeUpdate();
            this.error = cs.getBoolean(7);
            this.msgError = cs.getString(6);
            if (!error) {
                iduser = cs.getInt(3);
                name = cs.getString(4);
                token = cs.getString(5);
                bsession.add(new BeanSession(iduser, token,name, this.msgError, error));
                con.commit();
            } else {
                bsession.add(new BeanSession(0, "","", this.msgError, this.error));
                con.rollback();
            }
        }
        con.close();

        return bsession;
    }

    public List<BeanUser> createUser(Connection con, BeanUser bu) throws SQLException {
        List<BeanUser> bUser = new ArrayList<>();
        con.setAutoCommit(false);
        try (CallableStatement cs = con.prepareCall("CALL public.pr_creacion_usuario(?,?,?,?,?,?,?,?,?,?)")) {
            cs.setString(1, bu.getP_rut());
            cs.setString(2, bu.getP_nombre());
            cs.setString(3, bu.getP_direccion());
            cs.setString(4, bu.getP_telefono());
            cs.setInt(5, bu.getP_id_tipo_usuario());
            cs.setString(6, bu.getP_user());
            cs.setString(7, bu.getP_pass());
            cs.setNull(8, java.sql.Types.INTEGER);
            cs.setNull(9, java.sql.Types.VARCHAR);
            cs.setNull(10, java.sql.Types.BOOLEAN);
            cs.registerOutParameter(8, Types.INTEGER);
            cs.registerOutParameter(9, Types.VARCHAR);
            cs.registerOutParameter(10, Types.BOOLEAN);
            cs.executeUpdate();
            this.error = cs.getBoolean(10);
            this.msgError = cs.getString(9);
            if (!error) {
                bUser.add(new BeanUser(cs.getInt(8), this.msgError, error));
                con.commit();
            } else {
                bUser.add(new BeanUser(this.msgError, this.error));
                con.rollback();
            }
        }
        con.close();

        return bUser;
    }

    public List<BeanOrder> createReservation(Connection con, BeanOrder bo) throws SQLException {
        List<BeanOrder> bOrder = new ArrayList<>();
        con.setAutoCommit(false);
        try (CallableStatement cs = con.prepareCall("CALL public.pr_creacion_reserva(?,?,?,?,?,?,?,?,?)")) {
            cs.setString(1, bo.getP_token());
            cs.setInt(2, bo.getP_id_usuario());
            cs.setInt(3, bo.getP_id_pelicula());
            cs.setInt(4, bo.getP_numero_reserva());
            cs.setInt(5, bo.getP_id_estado());
            cs.setNull(6, java.sql.Types.INTEGER);
            cs.setNull(7, java.sql.Types.VARCHAR);
            cs.setNull(8, java.sql.Types.VARCHAR);
            cs.setNull(9, java.sql.Types.BOOLEAN);
            cs.registerOutParameter(6, Types.INTEGER);
            cs.registerOutParameter(7, Types.VARCHAR);
            cs.registerOutParameter(8, Types.VARCHAR);
            cs.registerOutParameter(9, Types.BOOLEAN);
            cs.executeUpdate();
            this.error = cs.getBoolean(9);
            this.msgError = cs.getString(8);
            if (!error) {
                bOrder.add(new BeanOrder(cs.getInt(6), cs.getString(7), this.msgError, error));
                con.commit();
            } else {
                bOrder.add(new BeanOrder(this.msgError, this.error));
                con.rollback();
            }
        }
        con.close();

        return bOrder;
    }

    public List<BeanSession> procInvalidateToken(Connection con, BeanSession bs) throws SQLException {
        List<BeanSession> bsession = new ArrayList<>();
        con.setAutoCommit(false);
        try (CallableStatement cs = con.prepareCall("CALL public.pr_logout(?,?,?,?)")) {
            cs.setInt(1, bs.getIdUser());
            cs.setString(2, bs.getToken());
            cs.setNull(3, java.sql.Types.VARCHAR);
            cs.setNull(4, java.sql.Types.BOOLEAN);
            cs.registerOutParameter(3, Types.VARCHAR);
            cs.registerOutParameter(4, Types.BOOLEAN);
            cs.executeUpdate();
            this.error = cs.getBoolean(4);
            this.msgError = cs.getString(3);
            if (!error) {
                bsession.add(new BeanSession(this.msgError, error));
                con.commit();
            } else {
                bsession.add(new BeanSession(this.msgError, this.error));
                con.rollback();
            }
        }
        con.close();

        return bsession;
    }

    public void procLoggerRecord(int errorCode, String msjError, String methodError, String classError) {
        Conex conn = new Conex();
        try {
            Connection con = conn.conection();
            con.setAutoCommit(false);

            try (CallableStatement cs = con.prepareCall("CALL public.pr_auditoria(?,?,?,?,?,?)")) {
                cs.setInt(1, errorCode);
                cs.setString(2, msjError);
                cs.setString(3, methodError);
                cs.setString(4, classError);
                cs.setNull(5, java.sql.Types.VARCHAR);
                cs.setNull(6, java.sql.Types.BOOLEAN);
                cs.registerOutParameter(5, Types.VARCHAR);
                cs.registerOutParameter(6, Types.BOOLEAN);
                cs.executeUpdate();
                this.error = cs.getBoolean(6);
                this.msgError = cs.getString(5);
                if (!error) {
                    con.commit();
                } else {
                    con.rollback();
                }
            }
            con.close();
        } catch (SQLException ex) {
            System.out.println("Error llamado Procedimiento auditoria " + ex.getMessage());
        } 
    }

}

<%-- 
    Document   : Order
    Created on : 17/11/2020, 6:57:12 p. m.
    Author     : 81001587
--%>

<%@page import="java.util.List"%>
<%@page import="Bean.BeanState"%>
<%@page import="Control.Controller"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    if (session.getAttribute("user") != null) {
        Controller c = new Controller();
        List<BeanState> bs = c.ListState(request);
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <br>
        <div class="panel-group" id="accordion">
            <div class="panel panel-danger">
                <div class="panel-heading headPanel tituloPrincipal">
                    <div class="row">
                        <div class=" col-sm-11 col-md-11 col-lg-11">
                            <a class="tituloa" data-toggle="collapse" aria-expanded="true" data-parent="#accordion" href="#collapse1">Consultar Reservas</a>
                        </div>
                        <div class=" col-sm-1 col-md-1 col-lg-1">
                            <a data-toggle="collapse" aria-expanded="true" data-parent="#accordion" href="#collapse1"><span class="collapsed">
                                    <img src="img/flecha_derecha.png" class="flechas" />
                                </span>
                                <span class="expanded">
                                    <img src="img/flecha_abajo.png" class="flechas" />
                                </span>
                            </a>
                        </div>
                    </div>
                </div>
                <div id="collapse1" class="panel-collapse collapse ">
                    <div class="panel-body">
                        <div class="row">
                            <div class="form-group  col-sm-3 col-md-3 col-lg-3   text-center">
                                <label for="corden" class="col-form-label">Orden</label>
                                <input class="form-control" type="text" id="corden" name="corden" value=""/>
                            </div>
                            <div class="form-group col-sm-3 col-md-3 col-lg-3   text-center">
                                <label for="state" class="col-form-label">Estado</label>
                                <select id="state"   class="form-control">
                                    <option value="">Seleccione...</option>
                                    <% for (int i = 0; i < bs.size(); i++) {%>
                                    <option value="<%=bs.get(i).getIdState()%>"><%=bs.get(i).getNameState()%></option>
                                    <%}%>
                                </select>
                            </div>
                            <div class="form-group col-sm-3 col-md-3 col-lg-3   text-center">
                                <label for="datest" class="col-form-label">Fecha Inicio</label>
                                <input class="form-control" type="date" id="datest" name="datest" value=""/>
                            </div>
                            <div class="form-group col-sm-3 col-md-3 col-lg-3   text-center">
                                <label for="dateend" class="col-form-label">Fecha Fin</label>
                                <input class="form-control" type="date" id="dateend" name="dateend" value=""/>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <div class="row">
                            <div class="form-group col-sm-5 col-md-5 col-lg-5  text-center"></div>
                            <div class="form-group col-sm-2 col-md-2 col-lg-2  text-center">
                                <button type="button" id="btOTConfirmar" class="btn search btn-sm" runat="server" onclick="Consultarorder()">                                    
                                    Consultar
                                </button>
                            </div>
                            <div class="form-group col-sm-5 col-md-5 col-lg-5  text-center" id="carg"></div>
                        </div>                       
                    </div>
                </div>
            </div>
        </div>

        <div class="panel panel-danger">
            <div class="panel-heading headPanel tituloPrincipal">
                Cat&aacute;logo
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-12 col-md-12 col-lg-12">
                        <table id="dtListOrder" class="display nowrap" style="width: 100%">
                            <thead>
                                <tr>
                                    <th>Orden</th>
                                    <th>Pel&iacute;cula</th>
                                    <th>Usuario</th>
                                    <th>Estado</th>
                                    <th>Ver</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div id="NewEditModal" class="modal col-md-offset-0  " style="overflow-y: scroll"  role="dialog" aria-hidden="true" aria-labelledby="NewEditModalTitle">
            <div class="modal-dialog  modal-lg"  role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="NewEditModalTitle">Detalle Reserva</h4>
                    </div>
                    <div class="modal-body">
                        <table id="dtListOrderdet" class="display nowrap" style="width: 100%">
                            <thead>
                                <tr>
                                    <th>Orden</th>
                                    <th>Estado Anterior</th>
                                    <th>Estado Actual</th>
                                    <th>Fecha Modificaci&oacute;n</th>
                                    <th>N&uacute;mero Prestadas</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                    <div class="modal-footer">
                        <div class="row text-center">
                            <div class="col-sm-5 col-md-5 col-lg-5 ">                                  </div>
                            <div class="col-sm-2 col-md-2 col-lg-2 ">
                                <button type="button" id="btCancelar" class="btn btn-danger btn-sm" onclick="javascript: $('#NewEditModal').modal('hide');">
                                    Salir
                                </button>
                            </div>
                            <div class="col-sm-5 col-md-5 col-lg-5 ">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
<%    } else {
        response.sendRedirect("index.jsp");
    }
%>

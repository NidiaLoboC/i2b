/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Services;

import Bean.BeanActor;
import Bean.BeanDirector;
import Bean.BeanMovies;
import Bean.BeanSelectMovies;
import Bean.BeanSession;
import Bean.BeanState;
import Bean.BeanUser;
import com.google.gson.Gson;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import com.google.gson.reflect.TypeToken;
import java.lang.reflect.Type;
import java.net.URL;
import java.util.List;

/**
 *
 * @author 81001587
 */
public class CalledServices {

    private int code;
    private int dirurl;
    private String typemethod;
    private String token;
    private int iduser;

    public CalledServices() {
    }

    public CalledServices(int dirurl, String typemethod, String token, int iduser) {
        this.dirurl = dirurl;
        this.typemethod = typemethod;
        this.token = token;
        this.iduser = iduser;
    }

    public List<BeanSession> login(String parameters) {
        List<BeanSession> Datos;
        Type listType = new TypeToken<List<BeanSession>>() {
        }.getType();
        Datos = new Gson().fromJson(CallWs(parameters), listType);

        return Datos;
    }

    public List<BeanUser> registeruUser(String parameters) {
        List<BeanUser> Datos;
        Type listType = new TypeToken<List<BeanUser>>() {
        }.getType();
        Datos = new Gson().fromJson(CallWs(parameters), listType);

        return Datos;
    }

    public List<BeanMovies> listMovie(String parameters) {
        List<BeanMovies> Datos;
        Type listType = new TypeToken<List<BeanMovies>>() {
        }.getType();
        Datos = new Gson().fromJson(CallWs(parameters), listType);

        return Datos;
    }

    public List<BeanActor> listActor(String parameters) {
        List<BeanActor> Datos;
        Type listType = new TypeToken<List<BeanActor>>() {
        }.getType();
        Datos = new Gson().fromJson(CallWs(parameters), listType);

        return Datos;
    }

    public List<BeanDirector> listDirector(String parameters) {
        List<BeanDirector> Datos;
        Type listType = new TypeToken<List<BeanDirector>>() {
        }.getType();
        Datos = new Gson().fromJson(CallWs(parameters), listType);

        return Datos;
    }

    public List<BeanState> listState(String parameters) {
        List<BeanState> Datos;
        Type listType = new TypeToken<List<BeanState>>() {
        }.getType();
        Datos = new Gson().fromJson(CallWs(parameters), listType);

        return Datos;
    }

    public String movie(String parameters) {
        return CallWs(parameters);
    }

    public List<BeanSession> logout(String parameters) {
        List<BeanSession> Datos;
        Type listType = new TypeToken<List<BeanSession>>() {
        }.getType();
        Datos = new Gson().fromJson(CallWs(parameters), listType);

        return Datos;
    }

    public String CallWs(String parameters) {
        String output = "";

        try {
            URL url = new URL("" + DirUrl(this.dirurl));
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            conn.setRequestMethod(this.typemethod);
            conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            if (this.token.length() > 0) {
                conn.setRequestProperty("token", this.token);
            }
            if (this.iduser > 0) {
                conn.setRequestProperty("iduser", this.iduser + "");
            }
            String input = parameters;
            OutputStream os = conn.getOutputStream();
            os.write(input.getBytes());
            os.flush();
            this.code = conn.getResponseCode();
            System.out.println("c " + this.code);
            if (conn.getResponseCode() == HttpURLConnection.HTTP_CREATED) {
                throw new RuntimeException("Fallo : HTTP error code : "
                        + conn.getResponseCode());
            }
            if (this.code == 200) {
                BufferedReader br = new BufferedReader(new InputStreamReader(
                        (conn.getInputStream())));
                int i = 0;
                output = br.readLine();
            }
            conn.disconnect();
        } catch (MalformedURLException e) {
            e.getMessage();
        } catch (IOException e) {
            e.getMessage();
        }
        System.out.println("output " + output);
        return output;
    }

    public String DirUrl(int code) {
        String direction = "";
        switch (code) {
            case 1:
                direction = "http://localhost:8080/WsPruebaI2b/ws/movie/login";
                break;
            case 2:
                direction = "http://localhost:8080/WsPruebaI2b/ws/movie/user";
                break;
            case 3:
                direction = "http://localhost:8080/WsPruebaI2b/ws/movie/listmovie";
                break;
            case 4:
                direction = "http://localhost:8080/WsPruebaI2b/ws/movie/actor";
                break;
            case 5:
                direction = "http://localhost:8080/WsPruebaI2b/ws/movie/director";
                break;
            case 6:
                direction = "http://localhost:8080/WsPruebaI2b/ws/movie/state";
                break;
        }
        return direction;

    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public int getDirurl() {
        return dirurl;
    }

    public void setDirurl(int dirurl) {
        this.dirurl = dirurl;
    }

    public String getTypemethod() {
        return typemethod;
    }

    public void setTypemethod(String typemethod) {
        this.typemethod = typemethod;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public int getIduser() {
        return iduser;
    }

    public void setIduser(int iduser) {
        this.iduser = iduser;
    }

}

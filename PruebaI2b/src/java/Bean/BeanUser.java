package Bean;

/**
 *
 * @author 81001587
 */
public class BeanUser {

    private int iduser;
    private String msgError;
    private boolean error;

    public BeanUser() {
    }

    public BeanUser(int iduser, String msgError, boolean error) {
        this.iduser = iduser;
        this.msgError = msgError;
        this.error = error;
    }

    public BeanUser(String msgError, boolean error) {
        this.msgError = msgError;
        this.error = error;
    }

    public int getIduser() {
        return iduser;
    }

    public void setIduser(int iduser) {
        this.iduser = iduser;
    }

    public String getMsgError() {
        return msgError;
    }

    public void setMsgError(String msgError) {
        this.msgError = msgError;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }
}

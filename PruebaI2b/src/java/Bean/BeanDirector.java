/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bean;

/**
 *
 * @author 81001587
 */
public class BeanDirector {

    private String token;
    private int idUser;
    private int idDirector;
    private String nameDirector; 
    private String msgError;
    private boolean error;

    public BeanDirector() {
    }

    public BeanDirector(String token, int idUser, int idDirector) {
        this.token = token;
        this.idUser = idUser;
        this.idDirector = idDirector;
    }

    public BeanDirector(int idDirector, String nameDirector) {
        this.idDirector = idDirector;
        this.nameDirector = nameDirector;
    }

    public BeanDirector(String msgError, boolean error) {
        this.msgError = msgError;
        this.error = error;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public int getIdUser() {
        return idUser;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    public int getIdDirector() {
        return idDirector;
    }

    public void setIdDirector(int idDirector) {
        this.idDirector = idDirector;
    }

    public String getNameDirector() {
        return nameDirector;
    }

    public void setNameDirector(String nameDirector) {
        this.nameDirector = nameDirector;
    }

    public String getMsgError() {
        return msgError;
    }

    public void setMsgError(String msgError) {
        this.msgError = msgError;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bean;

/**
 *
 * @author 81001587
 */
public class BeanState {

    private String token;
    private int idUser;
    private int idState;
    private String nameState;
    private String msgError;
    private boolean error;

    public BeanState() {
    }

    public BeanState(String token, int idUser, int idState) {
        this.token = token;
        this.idUser = idUser;
        this.idState = idState;
    }

    public BeanState(int idState, String nameState) {
        this.idState = idState;
        this.nameState = nameState;
    }

    public BeanState(String msgError, boolean error) {
        this.msgError = msgError;
        this.error = error;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public int getIdUser() {
        return idUser;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    public int getIdState() {
        return idState;
    }

    public void setIdState(int idState) {
        this.idState = idState;
    }

    public String getNameState() {
        return nameState;
    }

    public void setNameState(String nameState) {
        this.nameState = nameState;
    }

    public String getMsgError() {
        return msgError;
    }

    public void setMsgError(String msgError) {
        this.msgError = msgError;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

}

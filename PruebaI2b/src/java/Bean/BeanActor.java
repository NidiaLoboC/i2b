/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bean;

/**
 *
 * @author 81001587
 */
public class BeanActor {

    private String token;
    private int idUser;
    private int idMovie;
    private int idActor;
    private String nameActor;
    private String msgError;
    private boolean error;

    public BeanActor() {
    }

    public BeanActor(String token, int idUser, int idMovie, int idActor) {
        this.token = token;
        this.idUser = idUser;
        this.idMovie = idMovie;
        this.idActor = idActor;
    }

    public BeanActor(int idActor, String nameActor) {
        this.idActor = idActor;
        this.nameActor = nameActor;
    }

    public BeanActor(String msgError, boolean error) {
        this.msgError = msgError;
        this.error = error;
    }

    public String getNameActor() {
        return nameActor;
    }

    public void setNameActor(String nameActor) {
        this.nameActor = nameActor;
    }

    public int getIdMovie() {
        return idMovie;
    }

    public void setIdMovie(int idMovie) {
        this.idMovie = idMovie;
    }

    public int getIdActor() {
        return idActor;
    }

    public void setIdActor(int idActor) {
        this.idActor = idActor;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public int getIdUser() {
        return idUser;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    public String getMsgError() {
        return msgError;
    }

    public void setMsgError(String msgError) {
        this.msgError = msgError;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

}

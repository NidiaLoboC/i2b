package Bean;

import java.util.List;

/**
 *
 * @author 81001587
 */
public class BeanSelectMovies {

    private String token;
    private int idUser;
    private int idMovies;
    private int status;
    private int idActor;
    private int idDirector;
    private String msgError;
    private boolean error;
    private String nameMovie;
    private String descriptionMovie;
    private int numMovie;
    private int numAvailable;
    private int numBorrowed;
    private boolean availability;

    public BeanSelectMovies() {
    }

    public BeanSelectMovies(String msgError, boolean error) {
        this.msgError = msgError;
        this.error = error;
    }

    public BeanSelectMovies(String token, int idUser, int idMovies, int status, int idActor, int idDirector) {
        this.token = token;
        this.idUser = idUser;
        this.idMovies = idMovies;
        this.status = status;
        this.idActor = idActor;
        this.idDirector = idDirector;
    }

    public BeanSelectMovies(int idMovies, String nameMovie, String descriptionMovie, int numMovie, int numAvailable, int numBorrowed, boolean availability) {
        this.idMovies = idMovies;
        this.nameMovie = nameMovie;
        this.descriptionMovie = descriptionMovie;
        this.numMovie = numMovie;
        this.numAvailable = numAvailable;
        this.numBorrowed = numBorrowed;
        this.availability = availability;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public int getIdUser() {
        return idUser;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    public int getIdMovies() {
        return idMovies;
    }

    public void setIdMovies(int idMovies) {
        this.idMovies = idMovies;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getIdActor() {
        return idActor;
    }

    public void setIdActor(int idActor) {
        this.idActor = idActor;
    }

    public int getIdDirector() {
        return idDirector;
    }

    public void setIdDirector(int idDirector) {
        this.idDirector = idDirector;
    }

    public String getMsgError() {
        return msgError;
    }

    public void setMsgError(String msgError) {
        this.msgError = msgError;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public String getNameMovie() {
        return nameMovie;
    }

    public void setNameMovie(String nameMovie) {
        this.nameMovie = nameMovie;
    }

    public int getNumMovie() {
        return numMovie;
    }

    public void setNumMovie(int numMovie) {
        this.numMovie = numMovie;
    }

    public int getNumAvailable() {
        return numAvailable;
    }

    public void setNumAvailable(int numAvailable) {
        this.numAvailable = numAvailable;
    }

    public int getNumBorrowed() {
        return numBorrowed;
    }

    public void setNumBorrowed(int numBorrowed) {
        this.numBorrowed = numBorrowed;
    }

    public boolean isAvailability() {
        return availability;
    }

    public void setAvailability(boolean availability) {
        this.availability = availability;
    }

    public String getDescriptionMovie() {
        return descriptionMovie;
    }

    public void setDescriptionMovie(String descriptionMovie) {
        this.descriptionMovie = descriptionMovie;
    }

}

package Control;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import Bean.BeanActor;
import Bean.BeanDirector;
import Bean.BeanMovies;
import Bean.BeanSession;
import Bean.BeanState;
import Bean.BeanUser;
import Services.CalledServices;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author 81001587
 */
@WebServlet(urlPatterns = {"/Controller"})
public class Controller extends HttpServlet {

    HttpServletResponse res;
    CalledServices cs = new CalledServices();

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        res = response;
        ////PrintWriter out = response.getWriter();
        String accion = ((request.getParameter("action") != null) ? request.getParameter("action")
                : ((request.getParameter("actionreg") == null) ? "vacio" : request.getParameter("actionreg")));
        switch (accion) {
            case "getaccess":
                Access(request);
                break;
            case "getregister":
                Register(request);
                break;
            case "vacio":
            case "getout":
                getout(request, response);
                break;
            default:
                throw new AssertionError();
        }
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        res = response;
        String accion = ((request.getParameter("action") != null) ? request.getParameter("action")
                : ((request.getParameter("actionreg") == null) ? "vacio" : request.getParameter("actionreg")));
        switch (accion) {
            case "getregister":
                Register(request);
                break;
            case "getaccess":
                Access(request);
                break;
            case "vacio":
            case "getout":
                getout(request, response);
                break;
            default:
                throw new AssertionError();
        }
        processRequest(request, response);
    }

    protected void Access(HttpServletRequest request) throws IOException {
        boolean valido = false;
        String user = ((request.getParameter("us") == null) ? "" : request.getParameter("us"));
        String pass = ((request.getParameter("passw") == null) ? "" : request.getParameter("passw"));
        if (user.length() > 1 || pass.length() > 1) {
            cs = new CalledServices(1, "POST", "", 0);
            List<BeanSession> Datos = cs.login("user=" + user + "&pass=" + pass + "");
            valido = (cs.getCode() == 200);
            if (valido) {
                HttpSession sesion = request.getSession();
                sesion.removeAttribute("error");
                sesion.removeAttribute("msj");
                if (Datos.get(0).isError()) {
                    sesion.setAttribute("error", Datos.get(0).getMsgError());
                } else {
                    sesion.setAttribute("msj", Datos.get(0).getMsgError());
                    sesion.setAttribute("nameuser", Datos.get(0).getName());
                    sesion.setAttribute("user", Datos.get(0).getIdUser());
                    sesion.setAttribute("token", Datos.get(0).getToken());
                }
            }
        }
        if (!valido) {
            HttpSession sesion = request.getSession();
            sesion.setAttribute("error", "Usuario o contrase&ntilde;a incorrectos");
        }
        res.sendRedirect("index.jsp");
    }

    protected void Register(HttpServletRequest request) throws IOException {
        boolean valido = false;
        String rut = ((request.getParameter("rut") == null) ? "" : request.getParameter("rut"));
        String name = ((request.getParameter("name") == null) ? "" : request.getParameter("name"));
        String address = ((request.getParameter("address") == null) ? "" : request.getParameter("address"));
        String phone = ((request.getParameter("phone") == null) ? "" : request.getParameter("phone"));
        String user = ((request.getParameter("user") == null) ? "" : request.getParameter("user"));
        String pass = ((request.getParameter("pass") == null) ? "" : request.getParameter("pass"));
        if (rut.length() > 1 || name.length() > 1 || address.length() > 1 || user.length() > 1 || pass.length() > 1) {
            cs = new CalledServices(2, "POST", "", 0);
            List<BeanUser> Datos = cs.registeruUser("rut=" + rut + "&name=" + name + "&address=" + address + "&phone=" + phone
                    + "&typeuser=1&user=" + user + "&pass=" + pass + "");
            valido = (cs.getCode() == 200);
            if (valido) {
                HttpSession sesion = request.getSession();
                sesion.removeAttribute("error");
                sesion.removeAttribute("msj");
                if (Datos.get(0).isError()) {
                    sesion.setAttribute("error", Datos.get(0).getMsgError());
                } else {
                    sesion.setAttribute("msj", Datos.get(0).getMsgError());
                }
            }
        }
        if (!valido) {
            HttpSession sesion = request.getSession();
            sesion.setAttribute("error", "No se registro con  el usuario.");
        }
        res.sendRedirect("index.jsp");
    }

    public List<BeanMovies> ListMovie(HttpServletRequest request) throws IOException {
        List<BeanMovies> Datos = null;
        cs = new CalledServices(3, "POST", "", 0);
        Datos = cs.listMovie("idmovie");
        return Datos;
    }

    public List<BeanActor> ListActor(HttpServletRequest request) throws IOException {
        List<BeanActor> Datos = null;
        cs = new CalledServices(4, "POST", "", 0);
        Datos = cs.listActor("idmovie&idactor");
        return Datos;
    }

    public List<BeanDirector> ListDirector(HttpServletRequest request) throws IOException {
        List<BeanDirector> Datos = null;
        cs = new CalledServices(5, "POST", "", 0);
        Datos = cs.listDirector("iddirector");
        return Datos;
    }

    public List<BeanState> ListState(HttpServletRequest request) throws IOException {
        boolean valido = false;
        int iduser = Integer.parseInt(request.getSession().getAttribute("user").toString());
        String token = request.getSession().getAttribute("token").toString();
        List<BeanState> Datos = null;
        if (iduser > 0 || token.length() > 1) {
            cs = new CalledServices(6, "POST", token, iduser);
            Datos = cs.listState("idstate");
            valido = (cs.getCode() == 200);
        }
        return Datos;
    }

    protected void getout(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
        response.setHeader("Pragma", "no-cache");
        response.setHeader("Expires", "0");
        String user = ((request.getParameter("us") == null) ? "" : request.getParameter("us"));
        String pass = ((request.getParameter("passw") == null) ? "" : request.getParameter("passw"));
        if (user.length() > 1 || pass.length() > 1) {
            cs = new CalledServices(1, "POST", "", 0);
            List<BeanSession> Datos = cs.logout("user=" + user + "&pass=" + pass + "");
        }

        HttpSession session = request.getSession();
        if (session == null) {
            response.sendRedirect(request.getContextPath() + "/index.jsp");
            return;
        }
        session.removeAttribute("user");
        session.removeAttribute("token");
        session.invalidate();
        response.sendRedirect(request.getContextPath() + "/index.jsp");
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

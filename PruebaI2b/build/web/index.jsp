<%-- 
    Document   : index
    Created on : 12/11/2020, 6:36:27 p. m.
    Author     : 81001587
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Prueba I2b</title>
        <link href="css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="css/Style.css" rel="stylesheet" type="text/css"/>
        <script src="js/jquery-3.3.1.min.js" type="text/javascript"></script>
        <script src="https://unpkg.com/popper.js/dist/umd/popper.min.js" type="text/javascript"></script>
        <script src="js/bootstrap.js" type="text/javascript"></script>
        <script src="js/navbar.js" type="text/javascript"></script>
        <link href="css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="css/Style.css" rel="stylesheet" type="text/css"/>
        <script src="js/bootstrap.js" type="text/javascript"></script>
        <script src="js/bootstrap.min.js" type="text/javascript"></script>
        <link href="jquery.growl/jquery.growl.css" rel="stylesheet" type="text/css"/>
        <script src="jquery.growl/jquery.growl.js" type="text/javascript"></script>
        <script src="js/js/1.10.21_jquery.dataTables.min.js"></script>
        <script src="js/js/1.6.2_dataTables.buttons.min.js"></script>
        <script src="js/js/3.1.3_jszip.min.js"></script>
        <script src="js/js/0.1.53_pdfmake.min.js"></script>
        <script src="js/js/0.1.53_vfs_fonts.js"></script>
        <script src="js/js/1.6.2_buttons.html5.min.js"></script>
        <script src="js/js/buttons_1.6.2_buttons.print.min.js"></script>
        <script src="js/js/select_1.3.1_dataTables.select.min.js"></script>
        <link href="css/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css"/>
        <script src="js/js/jquery.dataTables.min.js" type="text/javascript"></script>
        <link href="css/jquery-ui-1.10.4.custom.min.css" rel="stylesheet" type="text/css"/>
        <script src="js/jquery-ui-1.10.4.custom.min.js" type="text/javascript"></script>
        <script src="js/ajax.js" type="text/javascript"></script>
    </head>
    <body id="inicio">
        <!--% HttpSession sesion=request.getSession(); 
        out.println("nueva: "+sesion.getMaxInactiveInterval()); %-->
        <input id="u" value="<%=((session.getAttribute("user") != null) ? session.getAttribute("user") : "")%>" type="hidden" />
        <input id="t"  value="<%=((session.getAttribute("token") != null) ? session.getAttribute("token") : "")%>" type="hidden" />
        <input id="error"  value="<%=((session.getAttribute("error") != null) ? session.getAttribute("error") : "")%>" type="hidden" />
        <input id="msj"  value="<%=((session.getAttribute("msj") != null) ? session.getAttribute("msj") : "")%>" type="hidden" />
        <div class="container process">
            <div class="row">
                <div class=" col-8 col-sm-8 col-xl-8 col-md-8 col-lg-8">
                    <img class="img-fluid" id="logo" src="img/logo.svg" alt="I2b"/>
                </div> 
                <div class=" col-4  col-sm-4 col-xl-4 col-md-4 col-lg-4"> 
                </div> 
            </div> 
            <br> 
            <div class="row">
                <div class=" col-lg-12 col-sm-12 col-md-12 col-xl-12 navegacion">
                    <ul class="nav nav-pills mb-3 " id="pills-tab" role="tablist" >
                        <li class="nav-item" >
                            <a class="nav-link seleccionbtn " id="pills-home-tab" data-toggle="pill" href="#" role="tab" name="conalista" aria-controls="pills-home" aria-selected="true">Cat&aacute;logo</a>
                        </li>
                        <% if (session.getAttribute("user") != null) {%>
                        <li class="nav-item" >
                            <a class="nav-link seleccionbtn " id="orden" data-toggle="pill" href="#" role="tab" name="orden" aria-selected="false">Consultar Reservas</a>
                        </li>
                        <li class="nav-item" >
                            <a class="nav-link seleccionbtn2 " id="logout" data-toggle="pill" href="#" role="tab" name="logout" aria-selected="false">Cerrar Sesi&oacute;n</a>
                        </li>
                        <%} else {%>
                        <li class="nav-item" >
                            <a class="nav-link seleccionbtn2 " id="iniciosess" data-toggle="pill" href="#" role="tab" name="iniciosess" aria-selected="false">Iniciar Sesi&oacute;n</a>
                        </li>  
                        <li class="nav-item" >
                            <a class="nav-link seleccionbtn " id="registrous" data-toggle="pill" href="#" role="tab" name="registrous" aria-selected="false">Reg&iacute;strate Aqu&iacute;</a>
                        </li>
                        <%}%>
                    </ul>
                </div> 
            </div>
            <div id="verdat">
                <%@include file="catalogue.jsp" %>
            </div>
            <div id="NewEditModal3" class="modal modal-lg col-md-offset-2" role="dialog" aria-hidden="true" aria-labelledby="NewEditModalTitle">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <img id="marca" src="img/marca.png" alt="I2b"/>
                            <h4 class="modal-title titulomarca" id="NewEditModalTitle">Iniciar Sesi&oacute;n</h4>
                        </div>
                        <div class="modal-body"> 
                            <form class="form inic" id="start" name="start" action="Controller" method="POST"  >

                                <div class="form-group">
                                    <label for="us" class="col-form-label">Usuario</label>
                                    <input class="form-control" type="text" required="" id="us" name="us" value=""/>
                                </div> 
                                <div class="form-group">
                                    <label for="password" class="col-form-label">Contrase&ntilde;a</label>
                                    <input class="form-control" type="password" required=""  id="passw" name="passw" value=""/>
                                </div>
                                <div class="form-group">
                                    <input type="hidden" id="action" name="action" value="getaccess"/>                                     
                                    <button type="button" id="btCancelarl" class="btn btn-danger btn-sm" onclick="javascript: $('#NewEditModal3').modal('hide');">
                                        Salir
                                    </button>   
                                    <input type="submit" class="btn search btn-sm" id="enviar" name="enviar"  value="Iniciar Sesión">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div id="NewEditModal4" class="modal modal-lg col-md-offset-2" role="dialog" aria-hidden="true" aria-labelledby="NewEditModalTitle">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <img id="marca" src="img/marca.png" alt="I2b"/>
                            <h4 class="modal-title titulomarca" id="NewEditModalTitle">Registrar Usuario en Prueba I2b</h4>
                        </div>
                        <div class="modal-body"> 
                            <form class="form inic" id="register" name="register" action="Controller" method="POST"  >
                                <div class="form-group">
                                    <label for="rut" class="col-form-label">Rut</label>
                                    <input class="form-control" required="" type="number" id="rut" name="rut" value=""/>
                                </div> 
                                <div class="form-group">
                                    <label for="name" class="col-form-label">Nombre Usuario</label>
                                    <input class="form-control" required="" type="text" id="name" name="name" value=""/>
                                </div>
                                <div class="form-group">
                                    <label for="address" class="col-form-label">Direcci&oacute;n </label>
                                    <input class="form-control" required="" type="text" id="address" name="address" value=""/>
                                </div> 
                                <div class="form-group">
                                    <label for="phone" class="col-form-label">Tel&eacute;fono</label>
                                    <input class="form-control" type="number" id="phone" name="phone" value=""/>
                                </div>                                
                                <div class="form-group">
                                    <label for="user" class="col-form-label">Usuario</label>
                                    <input class="form-control" required="" type="text" id="user" name="user" value=""/>
                                </div> 
                                <div class="form-group">
                                    <label for="pass" class="col-form-label">Contrase&ntilde;a</label>
                                    <input class="form-control" required="" type="password" id="pass" name="pass" value=""/>
                                </div>

                                <div class="form-group">
                                    <input type="hidden" id="actionreg" name="actionreg" value="getregister"/>
                                    <button type="button" id="btCancelar" class="btn btn-danger btn-sm" onclick="javascript: $('#NewEditModal4').modal('hide');">
                                        Salir
                                    </button>
                                    <input type="submit" class="btn search btn-sm" id="registro" name="registro" value="Guardar">

                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>      
    </body>
</html>

<%-- 
    Document   : catalogue
    Created on : 16/11/2020, 4:16:17 p. m.
    Author     : 81001587
--%>

<%@page import="Bean.BeanState"%>
<%@page import="Bean.BeanDirector"%>
<%@page import="Bean.BeanActor"%>
<%@page import="Bean.BeanMovies"%>
<%@page import="java.util.List"%>
<%@page import="Control.Controller"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
        Controller c = new Controller();
        List<BeanMovies> bm = c.ListMovie(request);
        List<BeanActor> ba = c.ListActor(request);
        List<BeanDirector> bd = c.ListDirector(request);
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <br>
        <div class="panel-group" id="accordion">
            <div class="panel panel-danger">
                <div class="panel-heading headPanel tituloPrincipal">
                    <div class="row">
                        <div class=" col-sm-11 col-md-11 col-lg-11">
                            <a class="tituloa" data-toggle="collapse" aria-expanded="true" data-parent="#accordion" href="#collapse1">Consultar Cat&aacute;logo
                            </a>
                        </div>
                        <div class=" col-sm-1 col-md-1 col-lg-1">
                            <a data-toggle="collapse" aria-expanded="true" data-parent="#accordion" href="#collapse1"><span class="collapsed">
                                    <img src="img/flecha_derecha.png" class="flechas" />
                                </span>
                                <span class="expanded">
                                    <img src="img/flecha_abajo.png" class="flechas" />
                                </span>
                            </a>
                        </div>
                    </div>
                </div>
                <div id="collapse1" class="panel-collapse collapse ">
                    <div class="panel-body">
                        <div class="row">
                            <div class="form-group  col-sm-4 col-md-4 col-lg-4   text-center">
                                <label for="listmovie" class="col-form-label">Pelicula</label>
                                <select id="listmovie"   class="form-control">
                                    <option value="">Seleccione...</option>
                                    <% for (int i = 0; i < bm.size(); i++) {%>
                                    <option value="<%=bm.get(i).getIdMovie()%>"><%=bm.get(i).getNameMovie()%></option>
                                    <%}%>
                                </select>
                            </div>
                            <div class="form-group col-sm-4 col-md-4 col-lg-4  text-center">
                                <label for="listactor" class="col-form-label">Actor</label>
                                <select id="listactor"   class="form-control">
                                    <option value="">Seleccione...</option>
                                    <% for (int i = 0; i < ba.size(); i++) {%>
                                    <option value="<%=ba.get(i).getIdActor()%>"><%=ba.get(i).getNameActor()%></option>
                                    <%}%>
                                </select>
                            </div>
                            <div class="form-group col-sm-4 col-md-4 col-lg-4  text-center">
                                <label for="listdirector" class="col-form-label">Director</label>
                                <select id="listdirector"   class="form-control">
                                    <option value="">Seleccione...</option>
                                    <% for (int i = 0; i < bd.size(); i++) {%>
                                    <option value="<%=bd.get(i).getIdDirector()%>"><%=bd.get(i).getNameDirector()%></option>
                                    <%}%>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <div class="row">
                            <div class="form-group col-sm-5 col-md-5 col-lg-5  text-center"></div>
                            <div class="form-group col-sm-2 col-md-2 col-lg-2  text-center">
                                <button type="button" id="btOTConfirmar" class="btn search btn-sm" runat="server" onclick="Consultarmovie()">                                    
                                    Consultar
                                </button>
                            </div>
                            <div class="form-group col-sm-5 col-md-5 col-lg-5  text-center" id="carg"></div>
                        </div>                       
                    </div>
                </div>
            </div>
        </div>

        <div class="panel panel-danger">
            <div class="panel-heading headPanel tituloPrincipal">
                Cat&aacute;logo
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-12 col-md-12 col-lg-12">
                        <table id="dtListCatalogo" class="display nowrap" style="width: 100%">
                            <thead>
                                <tr>
                                    <th>Pel&iacute;cula</th>
                                    <th>Descripci&oacute;n</th>
                                    <th>Stock</th>
                                    <th>Cantidad Disponibles</th>
                                    <th>Prestadas</th>                                    
                                        <% if (session.getAttribute("user") != null) {%> 
                                    <th>Ver</th>
                                        <%}%>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div id="NewEditModal" class="modal modal-lg col-md-offset-2" role="dialog" aria-hidden="true" aria-labelledby="NewEditModalTitle">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="NewEditModalTitle">Realizar Reserva</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="txtmovie" class="col-form-label">Pel&iacute;cula</label>
                            <input id="txtmovie" type="text" value=""  disabled="" readonly=""/>
                        </div> 
                        <div class="form-group">
                            <label for="numreserva" class="col-form-label">Cantidad Reservar</label>
                            <input id="numreserva" type="number" min="0"  value=""/>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="row text-center">
                            <div class="col-sm-5 col-md-5 col-lg-5 ">
                                <button type="button" id="btConfirmar" class="btn search btn-sm" onclick="saveRegistro(this.id);">
                                    <%--<span class="glyphicon glyphicon-floppy-save"></span>--%>
                                    Guardar
                                </button>
                            </div>
                            <div class="col-sm-2 col-md-2 col-lg-2 ">
                                <button type="button" id="btCancelar" class="btn btn-danger btn-sm" onclick="javascript: $('#NewEditModal').modal('hide');">
                                    Salir
                                </button>
                            </div>
                            <div class="col-sm-5 col-md-5 col-lg-5 ">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="NewEditModal2" class="modal modal-lg col-md-offset-2" role="dialog" aria-hidden="true" aria-labelledby="NewEditModalTitle">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="NewEditModalTitle">Orden de Reserva</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="numorder" class="col-form-label">N&uacute;mero Orden</label>
                            <input id="numorder" type="text" value=""  disabled="" readonly=""/>
                        </div> 
                        <div class="form-group">
                            <label for="nameuser" class="col-form-label">Nombre Usuario</label>
                            <input id="nameuser" type="text" min="0"  disabled="" 
                                   readonly="" value="<%=((session.getAttribute("nameuser")!=null)?session.getAttribute("nameuser"):"")%>"/>
                        </div>
                        <div class="form-group">
                            <label for="txtmovie2" class="col-form-label">Pel&iacute;cula</label>
                            <input id="txtmovie2" type="text" value=""  disabled="" readonly=""/>
                        </div> 
                        <div class="form-group">
                            <label for="numreserva2" class="col-form-label">Cantidad Reservar</label>
                            <input id="numreserva2" type="text" min="1"  disabled="" readonly="" value=""/>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="row text-center">
                            <div class="col-sm-5 col-md-5 col-lg-5 ">
                            </div>
                            <div class="col-sm-2 col-md-2 col-lg-2 ">
                                <button type="button" id="btCancelar" class="btn btn-danger btn-sm" onclick="javascript: $('#NewEditModal2').modal('hide');">
                                    Salir
                                </button>
                            </div>
                            <div class="col-sm-5 col-md-5 col-lg-5 ">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>


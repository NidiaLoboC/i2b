var htm = '<div  id="c"><b>Cargando</b>\n\
                                <br>\n\
                                 <img class="carga" src="img/cargando.gif" alt="cargando">\n\
                                </div>';
$(document).ready(function () {

    var error = $('#error').val();
    if (error.length > 1) {
        $.growl.error({title: "Alerta!", message: error});
        $('#error').val("");
    }
    var msj = $('#msj').val();
    if (msj.length > 1) {
        $.growl.notice({title: "Confirmación!", message: msj});
        $('#msj').val("");
    }
    Consultarmovie();
    var token = $('#t').val();
    if (token.length > 1) {
        Consultarorder();
    }
});

function saveRegistro(valid) {
    var id = $('#' + valid + '').data("tipoid");
    var name = $('#' + valid + '').data("name");
    var parametros = {
        idmovie: id,
        numreserv: $('#numreserva').val(),
        idstate: 3
    };
    $.ajax({
        type: 'POST',
        url: "http://localhost:8080/WsPruebaI2b/ws/movie/createorder",
        contentType: 'application/x-www-form-urlencoded',
        headers: {
            "token": $('#t').val(),
            "iduser": $('#u').val()
        },
        datatype: 'json',
        data: parametros,
        statusCode: {
            404: function () {
                $("#errores").html('<div class="col-lg-12 "><div class="alert alert-danger" role="alert">No se encuentra la página</div></div>');
            },
            500: function () {
                $("#errores").html('<div class="col-lg-12 "><div class="alert alert-danger" role="alert">Error en el servidor</div></div>');
            }
        },
        beforeSend: function () {
            $("#carg").html(htm);
        },
        success: function (data) {
            console.log(data);
            if (data[0]["error"]) {
                $.growl.error({title: "Alerta!", message: data[0]["msgError"]});
            }
//                else if (data["msgError"] != "") {
//                    $.growl.warning({ title: "Advertencia!", message: data.d.Mensaje });
//                }
            else {
                Consultarmovie();
                $("#numorder").val(data[0]["codOrder"]);
                $("#txtmovie2").val(name);
                var num = $('#numreserva').val();
                $('#numreserva2').val(num);

                $('#NewEditModal2').modal('show');
                $.growl.notice({title: "Confirmación!", message: data[0]["msgError"]});
            }
            $('#NewEditModal').modal('hide');
            $("#carg").html("");
        }
    });
}

function Consultarmovie() {
    var parametros = {
        idstate: 0,
        idmovie: $('#listmovie>option:selected').val(),
        idactor: $('#listactor>option:selected').val(),
        iddirector: $('#listdirector>option:selected').val()
    };
    $.ajax({
        type: 'POST',
        url: "http://localhost:8080/WsPruebaI2b/ws/movie",
        contentType: 'application/x-www-form-urlencoded',
//        headers: {
//            "token": $('#t').val(),
//            "iduser": $('#u').val()
//        },
        datatype: 'json',
        data: parametros,
        statusCode: {
            404: function () {
                $("#errores").html('<div class="col-lg-12 "><div class="alert alert-danger" role="alert">No se encuentra la página</div></div>');
            },
            500: function () {
                $("#errores").html('<div class="col-lg-12 "><div class="alert alert-danger" role="alert">Error en el servidor</div></div>');
            }
        },
        beforeSend: function () {
            $("#carg").html(htm);
        },
        success: function (data) {
//            console.log(data);
//            console.log(data[0]);
            var table;
            if ($.fn.DataTable.isDataTable('#dtListCatalogo')) {
                $('#dtListCatalogo').DataTable().clear();
            }
            var token = $('#t').val();
            if (token.length > 1) {
                table = $('#dtListCatalogo').DataTable({
                    destroy: true,
                    data: data,
                    columns: [
                        {'data': 'nameMovie'},
                        {'data': 'descriptionMovie'},
                        {'data': 'numMovie'},
                        {'data': 'numAvailable'},
                        {'data': 'numBorrowed'},
                        {
                            "data": null,
                            "sortable": false,
                            "render": function (data) {
                                return '<a id="btnVer' + data["idMovies"] + '" data-tipoid="' + data["idMovies"] + '"  data-name="' + data["nameMovie"] + '"'
                                        + ((data["availability"]) ? "" : 'disabled="disabled" title="Unidades no disponibles."')
                                        + ' class="btn search btn-xs botver" >Reservar</a>';
                            }
                        }

                    ],
                    "columnDefs": [{
                            "searchable": false,
                            "orderable": false,
                            "targets": 0
                        }],
                    /// sort at column three
                    "order": [[1, 'asc']],
                    dom: 'Bfrtip',
                    buttons: [
                        'copyHtml5',
                        'excelHtml5',
                        'csvHtml5',
                        'pdfHtml5',
                        {
                            extend: 'print',
                            autoPrint: false,
                            text: 'Print',
                            exportOptions: {
                                rows: function (idx, data, node) {
                                    //var dt = new $.fn.dataTable.Api('#dtListadoots');
                                    var selected = table.rows({selected: true}).indexes().toArray();
                                    //   var selected = table.rows('.selected').indexes().toArray();

                                    if (selected.length === 0 || $.inArray(idx, selected) !== -1)
                                        return true;
                                    return false;
                                }
                            }
                        }
                    ],
                    select: {
                        style: 'multi'
                    },
                    language: {
                        "decimal": "",
                        "emptyTable": "No hay información",
                        "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
                        "infoEmpty": "Mostrando 0 a 0 de 0 Entradas",
                        "infoFiltered": "(Filtrado de _MAX_ total entradas)",
                        "infoPostFix": "",
                        "thousands": ",",
                        "lengthMenu": "Mostrar _MENU_ Entradas",
                        "loadingRecords": "Cargando...",
                        "processing": "Procesando...",
                        "search": "Buscar:",
                        "zeroRecords": "Sin resultados encontrados",
                        "paginate": {
                            "first": "Primero",
                            "last": "Ultimo",
                            "next": "Siguiente",
                            "previous": "Anterior"
                        }
                    }
                });

                var btver = $('#dtListCatalogo tbody .botver');
                btver.on('click', function () {
                    var val = ($('.botver').attr('disabled'));
                    if (!val) {
                        var accion = $(this).data("tipoid");
                        var name = $(this).data("name");
                        var movie = $('#listmovie>option:selected').text();
                        $("#txtmovie").val(name);
                        $('#numreserva').val("");
                        $('#btConfirmar').attr('data-name', name);
                        $('#btConfirmar').attr('data-tipoid', accion);
                        $('#NewEditModal').modal('show');
                    }
                });
            } else {
                table = $('#dtListCatalogo').DataTable({
                    destroy: true,
                    data: data,
                    columns: [
                        {'data': 'nameMovie'},
                        {'data': 'descriptionMovie'},
                        {'data': 'numMovie'},
                        {'data': 'numAvailable'},
                        {'data': 'numBorrowed'}
                    ],
                    language: {
                        "decimal": "",
                        "emptyTable": "No hay información",
                        "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
                        "infoEmpty": "Mostrando 0 a 0 de 0 Entradas",
                        "infoFiltered": "(Filtrado de _MAX_ total entradas)",
                        "infoPostFix": "",
                        "thousands": ",",
                        "lengthMenu": "Mostrar _MENU_ Entradas",
                        "loadingRecords": "Cargando...",
                        "processing": "Procesando...",
                        "search": "Buscar:",
                        "zeroRecords": "Sin resultados encontrados",
                        "paginate": {
                            "first": "Primero",
                            "last": "Ultimo",
                            "next": "Siguiente",
                            "previous": "Anterior"
                        }
                    }
                });
            }
            $("#carg").html("");
        }
    });
}

function Consultarorder() {
    var estado = $('#state>option:selected').val();
    var ord = $('#corden').val();
    var di = $('#datest').val();
    var df = $('#dateend').val();
    var parametros = {
        idstate: ((estado > 0) ? estado : 0),
        idorder: ((ord != null) ? ord : null),
        dates: ((di != null) ? di : null),
        datef: ((df != null) ? df : null)
    };
    $.ajax({
        type: 'POST',
        url: "http://localhost:8080/WsPruebaI2b/ws/movie/order",
        contentType: 'application/x-www-form-urlencoded',
        headers: {
            "token": $('#t').val(),
            "iduser": $('#u').val()
        },
        datatype: 'json',
        data: parametros,
        statusCode: {
            404: function () {
                $("#errores").html('<div class="col-lg-12 "><div class="alert alert-danger" role="alert">No se encuentra la página</div></div>');
            },
            500: function () {
                $("#errores").html('<div class="col-lg-12 "><div class="alert alert-danger" role="alert">Error en el servidor</div></div>');
            }
        },
        beforeSend: function () {
            $("#carg").html(htm);
        },
        success: function (data) {
            console.log(data);
//            console.log(data[0]);
            var table;
            if ($.fn.DataTable.isDataTable('#dtListOrder')) {
                $('#dtListOrder').DataTable().clear();
            }
            table = $('#dtListOrder').DataTable({
                destroy: true,
                data: data,
                columns: [
                    {'data': 'order'},
                    {'data': 'movie'},
                    {'data': 'user'},
                    {'data': 'status'},
                    {
                        "data": null,
                        "sortable": false,
                        "render": function (data) {
                            return '<a id="btnVer' + data["idOrder"] + '" data-tipoid="' + data["idOrder"] + '"  '
                                    + ' class="btn search btn-xs botver" >Detalle</a>';
                        }
                    }

                ],
                "columnDefs": [{
                        "searchable": false,
                        "orderable": false,
                        "targets": 0
                    }],
                /// sort at column three
                "order": [[1, 'asc']],
                dom: 'Bfrtip',
                buttons: [
                    'copyHtml5',
                    'excelHtml5',
                    'csvHtml5',
                    'pdfHtml5',
                    {
                        extend: 'print',
                        autoPrint: false,
                        text: 'Print',
                        exportOptions: {
                            rows: function (idx, data, node) {
                                //var dt = new $.fn.dataTable.Api('#dtListadoots');
                                var selected = table.rows({selected: true}).indexes().toArray();
                                //   var selected = table.rows('.selected').indexes().toArray();

                                if (selected.length === 0 || $.inArray(idx, selected) !== -1)
                                    return true;
                                return false;
                            }
                        }
                    }
                ],
                select: {
                    style: 'multi'
                },
                language: {
                    "decimal": "",
                    "emptyTable": "No hay información",
                    "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
                    "infoEmpty": "Mostrando 0 a 0 de 0 Entradas",
                    "infoFiltered": "(Filtrado de _MAX_ total entradas)",
                    "infoPostFix": "",
                    "thousands": ",",
                    "lengthMenu": "Mostrar _MENU_ Entradas",
                    "loadingRecords": "Cargando...",
                    "processing": "Procesando...",
                    "search": "Buscar:",
                    "zeroRecords": "Sin resultados encontrados",
                    "paginate": {
                        "first": "Primero",
                        "last": "Ultimo",
                        "next": "Siguiente",
                        "previous": "Anterior"
                    }
                }
            });

            var btver = $('#dtListOrder tbody .botver');
            btver.on('click', function () {
                var accion = $(this).data("tipoid");
                console.log(accion);
                ConsultarorderDet(accion);
            });
            $("#carg").html("");
        }
    });
}

function ConsultarorderDet(accion) {
    $('#NewEditModal').modal('show');
    var parametros = {
        idorder: accion
    };
    $.ajax({
        type: 'POST',
        url: "http://localhost:8080/WsPruebaI2b/ws/movie/detail",
        contentType: 'application/x-www-form-urlencoded',
        headers: {
            "token": $('#t').val(),
            "iduser": $('#u').val()
        },
        datatype: 'json',
        data: parametros,
        statusCode: {
            404: function () {
                $("#errores").html('<div class="col-lg-12 "><div class="alert alert-danger" role="alert">No se encuentra la página</div></div>');
            },
            500: function () {
                $("#errores").html('<div class="col-lg-12 "><div class="alert alert-danger" role="alert">Error en el servidor</div></div>');
            }
        },
        beforeSend: function () {
            $("#carg").html(htm);
        },
        success: function (data) {
//            console.log(data);
//            console.log(data[0]);
            var table;
            if ($.fn.DataTable.isDataTable('#dtListOrderdet')) {
                $('#dtListOrderdet').DataTable().clear();
            }

            table = $('#dtListOrderdet').DataTable({
                destroy: true,
                data: data,
                columns: [
                    {'data': 'order'},
                    {'data': 'previousState'},
                    {'data': 'actualState'},
                    {'data': 'modificationDate'},
                    {'data': 'numBorrowedMovie'}

                ],
                "columnDefs": [{
                        "searchable": false,
                        "orderable": false,
                        "targets": 0
                    }],
                /// sort at column three
                "order": [[1, 'asc']],
                dom: 'Bfrtip',
                buttons: [
                    'copyHtml5',
                    'excelHtml5',
                    'csvHtml5',
                    'pdfHtml5',
                    {
                        extend: 'print',
                        autoPrint: false,
                        text: 'Print',
                        exportOptions: {
                            rows: function (idx, data, node) {
                                //var dt = new $.fn.dataTable.Api('#dtListadoots');
                                var selected = table.rows({selected: true}).indexes().toArray();
                                //   var selected = table.rows('.selected').indexes().toArray();

                                if (selected.length === 0 || $.inArray(idx, selected) !== -1)
                                    return true;
                                return false;
                            }
                        }
                    }
                ],
                select: {
                    style: 'multi'
                },
                language: {
                    "decimal": "",
                    "emptyTable": "No hay información",
                    "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
                    "infoEmpty": "Mostrando 0 a 0 de 0 Entradas",
                    "infoFiltered": "(Filtrado de _MAX_ total entradas)",
                    "infoPostFix": "",
                    "thousands": ",",
                    "lengthMenu": "Mostrar _MENU_ Entradas",
                    "loadingRecords": "Cargando...",
                    "processing": "Procesando...",
                    "search": "Buscar:",
                    "zeroRecords": "Sin resultados encontrados",
                    "paginate": {
                        "first": "Primero",
                        "last": "Ultimo",
                        "next": "Siguiente",
                        "previous": "Anterior"
                    }
                }
            });
            $("#carg").html("");
        }
    });
}
function cerrar() {
    var parametros = {
        action: "getout"
    };
    $.ajax({
        type: 'POST',
        url: "Controller",
        data: parametros,
        statusCode: {
            404: function () {
                $("#errores").html('<div class="col-lg-12 "><div class="alert alert-danger" role="alert">No se encuentra la página</div></div>');
            },
            500: function () {
                $("#errores").html('<div class="col-lg-12 "><div class="alert alert-danger" role="alert">Error en el servidor</div></div>');
            }
        },
        beforeSend: function () {
            $("#carg").html(htm);
        },
        success: function () {
            window.location.reload();
        }
    });
}




